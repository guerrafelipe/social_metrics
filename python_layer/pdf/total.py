import pdfkit
import pdf
import operator
import time, sys, os
from pymongo import MongoClient
import settings, utilities
from bs4 import BeautifulSoup as bs
import datetime
from datetime import date
from datetime import timedelta
import plotly.plotly as py
import plotly.graph_objs as go
from plotly.tools import FigureFactory as FF
from clint.textui import colored
from wordcloud import WordCloud
import matplotlib.pyplot as plt

def initialize(db, nickname):
    global email
    global fullname
    global fb_page_link
    global tw_user_name
    global fb_new_posts_count
    global fb_new_comments_count
    global fb_most_posted_type
    global fb_new_average_reactions_by_post
    global fb_new_average_comments_by_post
    global fb_new_average_shares_by_post
    global fb_new_average_likes_by_post
    global fb_new_average_sads_by_post
    global fb_new_average_angrys_by_post
    global fb_new_average_hahas_by_post
    global fb_new_average_wows_by_post
    global fb_new_average_loves_by_post
    global fb_new_percentage_status
    global fb_new_percentage_links
    global fb_new_percentage_videos
    global fb_new_percentage_photos
    global fb_new_comments_average_positivereacs
    global fb_new_comments_average_negativereacs
    global fb_new_photo_count
    global fb_new_photo_count
    global fb_new_link_count
    global fb_new_video_count 
    global fb_new_status_count
    global tw_new_average_retweets_count
    global tw_new_average_favorites_count
    global fan_count_dic
    global talking_about_count_dic
    global fb_fans_count
    global fb_talking_about_count
    global fb_most_fans_country
    global fb_less_fans_country
    global tw_followers_count
    global followers_count_dic
    global tw_followers_count
    global raw_comments
    global raw_retweets
    global tw_new_tweets_count
    global tw_new_retweets_count  
    global fb_new_status_type_popularity_dic
    global fb_new_sads_distribution
    global fb_new_angrys_distribution
    global fb_new_hahas_distribution
    global fb_new_wows_distribution
    global fb_new_likes_distribution
    global fb_new_loves_distribution
    global fb_new_reactions_distribution
    global fb_new_status_type_popularity_dic
    global fb_new_average_negative_comments_by_post
    global fb_new_average_positive_comments_by_post
    global fb_fans_country_distribution_dic
    global fb_words_fdist_comments
    global fb_most_important_post_type_for_popularity
    global fb_overall_reaction
    global fb_most_posted_day_of_week
    global fb_most_popular_day_of_week
    global tw_most_posts_day_of_week
    global tw_most_popular_day_of_week
    global fb_most_fans_country
    global fb_less_fans_country
    global fb_most_posted_type
    global fb_new_status_type_most_popular
    global fb_most_important_post_type_for_popularity
    global fb_overall_reaction
    global fb_most_posted_day_of_week
    global fb_most_popular_day_of_week
    global tw_most_posts_day_of_week
    global tw_most_popular_day_of_week
    
    global fb_years_list
    global fb_months_list
    global fb_days_of_week_list
    global fb_days_of_week_popularity_list
    global fb_most_posted_day_of_week
    global fb_most_posts_day_of_week
    global fb_most_popular_day_of_week

    global tw_years_list
    global tw_months_list
    global tw_days_of_week_list
    global tw_days_of_week_popularity_list
    global tw_most_posted_day_of_week
    global tw_most_popular_day_of_week
    
    fb_most_posts_day_of_week, tw_most_popular_day_of_week = "", ""
    tw_most_posted_day_of_week, tw_most_popular_day_of_week = "", ""
    fb_most_fans_country, fb_less_fans_country, fb_most_posted_type, fb_new_status_type_most_popular  = "", "", "", ""
    fb_most_important_post_type_for_popularity, fb_overall_reaction, fb_most_posted_day_of_week = "", "", ""
    fb_most_popular_day_of_week, tw_most_posts_day_of_week, tw_most_popular_day_of_week = "", "", ""
    tw_most_posts_day_of_week, tw_most_popular_day_of_week = "", ""
    fb_most_posted_day_of_week, fb_most_popular_day_of_week = "", ""
    fb_most_important_post_type_for_popularity, fb_overall_reaction = "", ""
    fb_most_fans_country, fb_less_fans_country, fb_most_posted_type = "", "", ""
    fb_new_posts_count, fb_new_comments_count = 0, 0
    fb_new_average_reactions_by_post, fb_new_average_comments_by_post, fb_new_average_shares_by_post = 0, 0, 0
    fb_new_average_likes_by_post, fb_new_average_sads_by_post = 0, 0
    fb_new_average_angrys_by_post, fb_new_average_hahas_by_post = 0, 0
    fb_new_average_wows_by_post, fb_new_average_loves_by_post = 0, 0
    fb_new_percentage_status, fb_new_percentage_links, fb_new_percentage_videos, fb_new_percentage_photos = 0, 0, 0, 0
    fb_new_comments_average_positivereacs, fb_new_comments_average_negativereacs, fb_new_status_count = 0, 0, 0
    fb_new_photo_count, fb_new_photo_count, fb_new_link_count, fb_new_video_count = 0, 0, 0, 0
    tw_new_average_retweets_count, tw_new_average_favorites_count = 0, 0

    user = db["users"].find({"nickname" : nickname})
    for u in user:
        email = u["email"]
        fullname = u["fullname"]
        fb_page_link = u["fb_page_link"]
        tw_user_name = "@" + u["tw_scr_name"]

    last_ready_statistic_fb = db["ready_statistics"].find({"social_network" : "facebook", "user_nickname" : nickname, "created_at" : time.strftime("%Y-%m-%d")})

    for r in last_ready_statistic_fb:
        fb_new_posts_count = r["posts_count"]
        fb_new_comments_count = r["comments_count"]
        fb_new_average_positive_comments_by_post = r["comments_average_positivereacs"]
        fb_new_average_negative_comments_by_post = r["comments_average_negativereacs"]
        fb_new_average_reactions_by_post = r["average_reactions_count"]
        fb_new_average_comments_by_post = r["average_comments_count"]
        fb_new_average_shares_by_post = r["average_shares_count"]
        fb_new_percentage_status = r["percentage_status"]
        fb_new_percentage_links = r["percentage_links"]
        fb_new_percentage_videos = r["percentage_videos"]
        fb_new_percentage_photos = r["percentage_photos"]
        fb_new_photo_count = r["photo_count"]
        fb_new_link_count = r["link_count"]
        fb_new_video_count = r["video_count"]
        fb_new_status_count = r["status_count"]
        fb_fans_country_distribution_dic = r["fans_country_distribution"]["value"]
        fb_words_fdist_comments = r["words_fdist_comments"]
        
        #Popularidade para cada tipo de post e influencia de cada tipo de post na reacao positiva, negativa
        fb_new_sads_distribution = r["sads_distribution"]
        fb_new_angrys_distribution = r["angrys_distribution"]
        fb_new_hahas_distribution = r["hahas_distribution"]
        fb_new_wows_distribution = r["wows_distribution"]
        fb_new_likes_distribution = r["likes_distribution"]
        fb_new_loves_distribution = r["loves_distribution"]
        fb_new_reactions_distribution = r["reactions_distribution"]
        fb_new_status_type_popularity_dic = r["status_type_popularity"] #is a dic
      
        #reactions
        fb_new_average_likes_by_post_dic = r["likes_distribution"]
        fb_new_average_sads_by_post_dic = r["sads_distribution"]
        fb_new_average_angrys_by_post_dic = r["angrys_distribution"]
        fb_new_average_hahas_by_post_dic = r["hahas_distribution"]
        fb_new_average_wows_by_post_dic = r["wows_distribution"]
        fb_new_average_loves_by_post_dic = r["loves_distribution"]
        for k in fb_new_average_likes_by_post_dic.values():
            fb_new_average_likes_by_post += k
        fb_new_average_likes_by_post /= fb_new_posts_count
      
        for k in fb_new_average_sads_by_post_dic.values():
            fb_new_average_sads_by_post += k
        fb_new_average_sads_by_post /= fb_new_posts_count
      
        for k in fb_new_average_angrys_by_post_dic.values():
            fb_new_average_angrys_by_post += k
        fb_new_average_angrys_by_post /= fb_new_posts_count
      
        for k in fb_new_average_hahas_by_post_dic.values():
            fb_new_average_hahas_by_post += k
        fb_new_average_hahas_by_post /= fb_new_posts_count
      
        for k in fb_new_average_wows_by_post_dic.values():
            fb_new_average_wows_by_post += k
        fb_new_average_wows_by_post /= fb_new_posts_count
      
        for k in fb_new_average_loves_by_post_dic.values():
            fb_new_average_loves_by_post += k
        fb_new_average_loves_by_post /= fb_new_posts_count
      
        #comments
        fb_new_comments_average_positivereacs = r["comments_average_positivereacs"]
        fb_new_comments_average_negativereacs = r["comments_average_negativereacs"]
      
        #post's distribution    
    most_fans_country = max(fb_fans_country_distribution_dic, key = fb_fans_country_distribution_dic.get)
    most_fans_country = utilities.get_country_name(most_fans_country)
    most_fans_country = ' '.join([w.capitalize() for w in most_fans_country.split()])
    fb_most_fans_country = most_fans_country
    less_fans_country = min(fb_fans_country_distribution_dic, key = fb_fans_country_distribution_dic.get)
    less_fans_country = utilities.get_country_name(less_fans_country)
    less_fans_country = ' '.join([w.capitalize() for w in less_fans_country.split()])
    fb_less_fans_country = less_fans_country
    percentage_list = [fb_new_percentage_status, fb_new_percentage_links, fb_new_percentage_videos, fb_new_percentage_photos]
    max_percentage_item = max(percentage_list)
    max_percentage_index = percentage_list.index(max_percentage_item)
    if len([k for k in percentage_list if k == max_percentage_item]) == 1:
        if max_percentage_index == 0:
            fb_most_posted_type = "status"
        elif max_percentage_index == 1:
            fb_most_posted_type = "links"          
        elif max_percentage_index == 2:
            fb_most_posted_type = "videos"   
        else:
           fb_most_posted_type = "fotos"
    else:
            fb_most_posted_type = "todos igualmente"
    fb_new_status_type_most_popular = max(fb_new_status_type_popularity_dic, key = fb_new_status_type_popularity_dic.get)
    fb_most_important_post_type_for_popularity = utilities.translate_post_type(fb_new_status_type_most_popular)
    positive_reputation = (fb_new_average_likes_by_post + fb_new_average_loves_by_post + fb_new_average_hahas_by_post + fb_new_average_wows_by_post) / fb_new_average_reactions_by_post
    positive_reputation *= 100   
    negative_reputation = (fb_new_average_angrys_by_post + fb_new_average_sads_by_post) / fb_new_average_reactions_by_post
    negative_reputation *= 100
   
    if positive_reputation > negative_reputation:
        fb_overall_reaction = "positiva"
    elif positive_reputation < negative_reputation:
        fb_overall_reaction = "negativa"
    else:
        fb_overall_reaction = "neutra"
   
    #Post distribution by periods of time
    posts = db["facebook_posts"].find({"user_nickname" : nickname})   
    #yesars_dic = [{2012 : 0}, {2013 : 0}, {2014 : 0}, {2015 : 0}, {2016 : 0}]
    #months_dic = [{2012/01 : 0}, {2012/02 : 0}, {2012/03 : 0}, {2012/03 : 0}, {2012/04 : 0}]
    #days_of_week_dic = [{monday : 0}, {tuesday : 0}, {thursday : 0}, {wednesday : 0}]   
    days_of_week_translation = { "Monday" : "Segunda-feira", "Tuesday" : "Terca-feira", "Wednesday" : "Quarta-feira", "Thursday" : "Quinta-feira", "Friday" : "Sexta-feira", "Saturday" : "Sabado", "Sunday" : "Domingo"}
    fb_years_list, fb_months_list, fb_days_of_week_list = [], [], []
    fb_en_days_of_week_list, fb_en_days_of_week_popularity_list = [], []
    fb_days_of_week_popularity_list = []
    #posts = posts[:10]

    for p in posts:
        created_date = p["status_created_date"]
        created_year = created_date.split("-")[0] 
        created_month = created_date.split("-")[1]
        created_day = created_date.split("-")[2]
        year_month = created_year + "/" + created_month
        years_dic, months_dic, en_days_of_week_dic = {}, {}, {}
        
        #Year's dic
        #[{u'2012': 211}, {u'2013': 179}, {u'2011': 215}, {u'2010': 288}, {u'2015': 68}, {u'2016': 76}, {u'2014': 131}, {u'2009': 55}, {u'1974': 1}]
        filtered_list = filter(lambda a : a.keys()[0] == created_year, fb_years_list)
        if len(filtered_list) == 1:     
             fb_years_list[fb_years_list.index(filtered_list[0])][created_year] = fb_years_list[fb_years_list.index(filtered_list[0])][created_year] + 1  
        else:
            years_dic[created_year] =  1
            fb_years_list.append(years_dic)
        
        #Month's dic
        #[{u'2012/09': 22}, {u'2012/10': 28}, {u'2012/04': 15}, {u'2013/03': 12}, {u'2011/04': 26}, {u'2010/10': 23}, {u'2013/04': 21}, {u'2011/06': 21}, {u'2012/08': 22}, {u'2015/12': 9}, {u'2011/03': 16}, {u'2013/10': 19}, {u'2015/06': 4}, {u'2016/03': 10}, {u'2011/01': 14}, {u'2011/08': 19}, {u'2010/12': 21}, {u'2014/07': 17}, {u'2011/10': 21}, {u'2013/01': 11}, {u'2012/02': 12}, {u'2013/06': 20}, {u'2011/05': 18}, {u'2010/09': 31}, {u'2010/07': 27}, {u'2014/05': 11}, {u'2016/08': 9}, {u'2013/05': 22}, {u'2011/11': 15}, {u'2010/08': 30}, {u'2016/04': 7}, {u'2015/08': 9}, {u'2011/12': 9}, {u'2010/01': 18}, {u'2016/06': 10}, {u'2013/12': 16}, {u'2013/02': 13}, {u'2012/07': 21}, {u'2013/11': 6}, {u'2011/09': 18}, {u'2010/11': 21}, {u'2016/01': 8}, {u'2009/11': 16}, {u'2012/01': 6}, {u'2014/08': 10}, {u'2010/03': 17}, {u'2015/11': 6}, {u'2009/10': 8}, {u'2015/10': 9}, {u'2012/11': 17}, {u'2013/08': 16}, {u'2012/05': 13}, {u'2016/07': 15}, {u'2014/12': 5}, {u'2010/04': 20}, {u'2011/02': 16}, {u'2016/02': 7}, {u'2013/07': 16}, {u'2012/06': 22}, {u'2014/01': 9}, {u'2010/06': 32}, {u'2014/06': 13}, {u'2014/03': 12}, {u'2016/05': 8}, {u'2011/07': 22}, {u'2010/05': 33}, {u'2014/11': 13}, {u'2015/04': 5}, {u'2014/10': 11}, {u'2012/03': 15}, {u'2013/09': 7}, {u'2015/03': 1}, {u'2010/02': 15}, {u'2014/09': 12}, {u'2015/07': 9}, {u'2012/12': 18}, {u'2015/09': 8}, {u'2014/02': 7}, {u'2009/06': 5}, {u'2015/05': 5}, {u'2009/09': 5}, {u'2009/12': 15}, {u'2014/04': 11}, {u'2015/01': 2}, {u'2015/02': 1}, {u'2009/08': 2}, {u'2016/09': 2}, {u'1974/04': 1}, {u'2009/07': 4}]
        filtered_list = filter(lambda a : a.keys()[0] == year_month, fb_months_list)
        if len(filtered_list) == 1:
            fb_months_list[fb_months_list.index(filtered_list[0])][year_month] =  fb_months_list[fb_months_list.index(filtered_list[0])][year_month] + 1
        else:
            months_dic[year_month] = 1
            fb_months_list.append(months_dic)
       
        #day_of_week
        year, month, day = (int(x) for x in created_date.split('-'))    
        ans = datetime.date(year, month, day)
        day_of_week = ans.strftime("%A")
        
        #days_of_week_dic
        filtered_list = filter(lambda a : a.keys()[0] == day_of_week, fb_en_days_of_week_list)
        if len(filtered_list) == 1:
            fb_en_days_of_week_list[fb_en_days_of_week_list.index(filtered_list[0])][day_of_week] = fb_en_days_of_week_list[fb_en_days_of_week_list.index(filtered_list[0])][day_of_week] + 1   
        else:
            en_days_of_week_dic[day_of_week] = 1
            fb_en_days_of_week_list.append(en_days_of_week_dic)
        
        filtered_list = filter(lambda a : a.keys()[0] == day_of_week, fb_en_days_of_week_popularity_list)
        if len(filtered_list) == 1:
            fb_en_days_of_week_popularity_list[fb_en_days_of_week_popularity_list.index(filtered_list[0])][day_of_week] = fb_en_days_of_week_popularity_list[fb_en_days_of_week_popularity_list.index(filtered_list[0])][day_of_week] + 1 
        else:
            en_days_of_week_dic[day_of_week] = 1
            fb_en_days_of_week_popularity_list.append(en_days_of_week_dic)
        #en_days_of_week_dic[day_of_week] = en_days_of_week_dic[day_of_week] + 1 if day_of_week in en_days_of_week_dic else 1        
        #en_days_of_week_popularity_dic[day_of_week] = en_days_of_week_popularity_dic[day_of_week] + (p["status_num_reactions"] + p["status_num_comments"] + p["status_num_shares"]) / 1000 if day_of_week in en_days_of_week_popularity_dic else (p["status_num_reactions"] + p["status_num_comments"] + p["status_num_shares"]) / 1000
    
    #Test
    #keys_list = [a.keys()[0].split("/")[0] for a in fb_months_list]
    #years = ["2016", "2015", "2014", "2013", "2012", "2011", "2010", "2009"]
    #y_count = []
    #for y in years:
    #    keys_list_year = filter(lambda a : a == y, keys_list) 
    #    y_count.append(len(keys_list_year))   
    #print(colored.green(y_count))  
    
    #[{u'2012': 211}, {u'2013': 179}, {u'2011': 215}, {u'2010': 288}, {u'2015': 68}, {u'2016': 76}, {u'2014': 131}, {u'2009': 55}, {u'1974': 1}]        
    #TODO: Filtrar datas para maior que 2010 (senao a escala do grafico fica zuada)
    fb_years_list = sorted(fb_years_list, key = lambda k : k.keys()[0])

    days_of_week_indexes = [("Segunda-feira", 0), ("Terca-feira", 1), ("Quarta-feira", 2), ("Quinta-feira", 3), ("Sexta-feira", 4), ("Sabado", 5), ("Domingo", 6)]
    unsorted_fb_days_of_week_list = []
    for k in fb_en_days_of_week_list:
        dic = {}
        pt_day_of_week = days_of_week_translation[k.keys()[0]]
        dic[pt_day_of_week] = k.values()[0]
        unsorted_fb_days_of_week_list.append(dic)
    
    unsorted_fb_days_of_week_popularity_list = []
    for k in fb_en_days_of_week_popularity_list:
        dic = {}
        pt_day_of_week = days_of_week_translation[k.keys()[0]]
        dic[pt_day_of_week] = k.values()[0]
        unsorted_fb_days_of_week_popularity_list.append(dic)
    
    #sort
    for k in days_of_week_indexes:
        result = filter(lambda a : a.keys()[0] == k[0], unsorted_fb_days_of_week_list)[0]
        fb_days_of_week_list.append(result)        
        result = filter(lambda a : a.keys()[0] == k[0], unsorted_fb_days_of_week_popularity_list)[0]
        fb_days_of_week_popularity_list.append(result)
   
    #MostPostsDayOfWeek
    fb_most_posts_day_of_week = max(fb_days_of_week_list, key = lambda a : a.values()[0]).keys()[0]
    fb_most_posted_day_of_week = fb_most_posts_day_of_week

    #MostPopularDayOfWeek
    fb_most_popular_day_of_week = max(fb_days_of_week_popularity_list, key = lambda a : a .values()[0]).keys()[0]
    fb_most_popular_day_of_week = fb_most_popular_day_of_week
    
    #Post distribution by periods of time
    posts = db["twitter_posts"].find({"user_nickname" : nickname})     
    tw_years_list, tw_months_list, tw_days_of_week_list = [], [], []
    tw_en_days_of_week_list, tw_en_days_of_week_popularity_list = [], []
    tw_days_of_week_popularity_list = []

    for p in posts:
        created_date = p["created_at"]
        created_year = created_date.split("-")[0] 
        created_month = created_date.split("-")[1]
        created_day = created_date.split("-")[2]
        year_month = created_year + "/" + created_month
        years_dic, months_dic, en_days_of_week_dic = {}, {}, {}
        
        #Year's dic
        filtered_list = filter(lambda a : a.keys()[0] == created_year, tw_years_list)
        if len(filtered_list) == 1:     
             tw_years_list[tw_years_list.index(filtered_list[0])][created_year] = tw_years_list[tw_years_list.index(filtered_list[0])][created_year] + 1  
        else:
            years_dic[created_year] =  1
            tw_years_list.append(years_dic)

        #Month's dic
        filtered_list = filter(lambda a : a.keys()[0] == year_month, tw_months_list)
        if len(filtered_list) == 1:
            tw_months_list[tw_months_list.index(filtered_list[0])][year_month] = tw_months_list[tw_months_list.index(filtered_list[0])][year_month] + 1
        else:
            months_dic[year_month] = 1
            tw_months_list.append(months_dic)

        #day_of_week
        year, month, day = (int(x) for x in created_date.split('-'))    
        ans = datetime.date(year, month, day)
        day_of_week = ans.strftime("%A")
        
        #days_of_week_dic
        filtered_list = filter(lambda a  : a.keys()[0] == day_of_week, tw_en_days_of_week_list)
        if len(filtered_list) == 1:
            tw_en_days_of_week_list[tw_en_days_of_week_list.index(filtered_list[0])][day_of_week] = tw_en_days_of_week_list[tw_en_days_of_week_list.index(filtered_list[0])][day_of_week] + 1   
        else:
            en_days_of_week_dic[day_of_week] = 1
            tw_en_days_of_week_list.append(en_days_of_week_dic)
        
        filtered_list = filter(lambda a : a.keys()[0] == day_of_week, tw_en_days_of_week_popularity_list)
        if len(filtered_list) == 1:
            tw_en_days_of_week_popularity_list[tw_en_days_of_week_popularity_list.index(filtered_list[0])][day_of_week] = tw_en_days_of_week_popularity_list[tw_en_days_of_week_popularity_list.index(filtered_list[0])][day_of_week] + 1 
        else:
            en_days_of_week_dic[day_of_week] = 1
            tw_en_days_of_week_popularity_list.append(en_days_of_week_dic)
        #en_days_of_week_dic[day_of_week] = en_days_of_week_dic[day_of_week] + 1 if day_of_week in en_days_of_week_dic else 1        
        #en_days_of_week_popularity_dic[day_of_week] = en_days_of_week_popularity_dic[day_of_week] + (p["status_num_reactions"] + p["status_num_comments"] + p["status_num_shares"]) / 1000 if day_of_week in en_days_of_week_popularity_dic else (p["status_num_reactions"] + p["status_num_comments"] + p["status_num_shares"]) / 1000
   
    tw_years_list = sorted(tw_years_list, key = lambda k : k.keys()[0]) 
    unsorted_tw_days_of_week_list = []
    for k in tw_en_days_of_week_list:
        dic = {}
        pt_day_of_week = days_of_week_translation[k.keys()[0]]
        dic[pt_day_of_week] = k.values()[0]
        unsorted_tw_days_of_week_list.append(dic)
     
    unsorted_tw_days_of_week_popularity_list = []
    for k in tw_en_days_of_week_popularity_list:
        dic = {}
        pt_day_of_week = days_of_week_translation[k.keys()[0]]
        dic[pt_day_of_week] = k.values()[0]
        unsorted_tw_days_of_week_popularity_list.append(dic)

    #sort
    for k in days_of_week_indexes:
        result = filter(lambda a : a.keys()[0] == k[0], unsorted_tw_days_of_week_list)[0]
        tw_days_of_week_list.append(result)        
        result = filter(lambda a : a.keys()[0] == k[0], unsorted_tw_days_of_week_popularity_list)[0]
        tw_days_of_week_popularity_list.append(result)
    

    #MostPostsDayOfWeek
    tw_most_posts_day_of_week = max(tw_days_of_week_list, key = lambda a : a.values()[0]).keys()[0]
    tw_most_posted_day_of_week = tw_most_posts_day_of_week
    
    #MostPopularDayOfWeek
    tw_most_popular_day_of_week = max(tw_days_of_week_popularity_list, key = lambda a : a .values()[0]).keys()[0]

    last_ready_statistics_tw = db["ready_statistics"].find({"social_network" : "twitter", "user_nickname" : nickname, "created_at" : time.strftime("%Y-%m-%d") })
    tw_new_tweets_count, tw_new_retweets_count = 0, 0    
    for r in last_ready_statistics_tw:
        tw_new_tweets_count = r["posts_count"]  
        tw_new_average_favorites_count = r["average_favorites_count"]
        tw_new_average_retweets_count = r["average_retweets_count"]
        tw_new_comments_average_positivereacs = r["comments_average_positivereacs"]
        tw_new_comments_average_negativereacs = r["comments_average_negativereacs"]
        tw_new_comments_averageneutralreacs = r["comments_average_neutralreacs"]

    tw_new_retweets_count = db["twitter_posts"].find({"user_nickname" : nickname, "is_retweet" : True}).count()
    about_collection_fb = db["about_collections"].find({"social_network" : "facebook", "user_nickname" : nickname})
    
    fan_count_dic, talking_about_count_dic = [], []
    fb_fans_count, fb_talking_about_count = 0, 0
    for r in about_collection_fb:
        #it's actualy a list of dics
        fan_count_dic = r["fan_count_list"]
        talking_about_count_dic = r["talking_about_count_list"]
        fb_fans_count = fan_count_dic[-1][time.strftime("%Y-%m-%d")]
        fb_talking_about_count = talking_about_count_dic[-1][time.strftime("%Y-%m-%d")]
      
    about_collection_tw = db["about_collections"].find({"social_network" : "twitter", "user_nickname" : nickname})
    followers_count_dic = []
    tw_followers_count = 0
    for r in about_collection_tw:
        followers_count_dic = r["followers_count_list"]
        tw_followers_count = followers_count_dic[-1][time.strftime("%Y-%m-%d")]
   
    #Get all comments of all facebook posts and join a string
    raw_comments = ""
    all_posts = db["facebook_posts"].find({ "user_nickname" : nickname })
    for p in all_posts:
        for c in p["comments"]:
            raw_comments += c["message"]
  
    raw_retweets = ""
    all_tweets = db["twitter_posts"].find({"user_nickname" : nickname})
    for p in all_tweets:
        for c in p["retweets_list"]:
            raw_retweets += c["text"]

def scrap_cover(db, nickname):
   file1 = open("html_pages/total/cover.html", "rb")
   html = file1.read()
   file1.close()
   soup = bs(html, "lxml")
   full_name_em = soup.find(id="userFullname")
   full_name_em.insert(0, fullname)
   tw_user_name_em = soup.find(id="twUserName")
   tw_user_name_em.insert(0, tw_user_name)
   page_link_em = soup.find(id="pageLink")
   page_link_em.insert(0, fb_page_link)   
   file1 = open("html_pages/temporary/cover_" + nickname + ".html", "wb")
   file1.write(soup.prettify())
   file1.close()

def scrap_introduction(db, nickname):
   file1 = open("html_pages/total/introduction.html")
   html = file1.read()
   file1.close()
   soup = bs(html, "lxml")
   #generatedDate
   generated_date_em = soup.find(id="generatedDate")
   generated_date_em.insert(0, time.strftime("%d/%m/%Y"))
   #generatedHour
   generated_hour_em = soup.find(id="generatedHour")
   generated_hour_em.insert(0, time.strftime("%H:%M:%S"))
   #totalFbPosts
   post_count_em = soup.find(id="totalFbPosts")
   post_count_em.insert(0, str(fb_new_posts_count))
   #totalFbComments
   fb_comments_count_em = soup.find(id="totalFbComments")
   fb_comments_count_em.insert(0, str(fb_new_comments_count))
   #totalTweets
   tw_total_tweets_em = soup.find(id="totalTweets")
   tw_total_tweets_em.insert(0, str(tw_new_tweets_count))
   #totalRetweets
   tw_total_retweets_em = soup.find(id = "totalRetweets")
   tw_total_retweets_em.insert(0, str(tw_new_retweets_count))
   file1 = open("html_pages/temporary/introduction_" + nickname + ".html", "wb")
   file1.write(soup.prettify())
   file1.close()

def scrap_overview(db, nickname):
   file1 = open("html_pages/total/overview.html", "rb")
   html = file1.read()
   file1.close()
   soup = bs(html, "lxml")
   #FbFansCount
   fb_fans_count_em = soup.find(id = "FbFansCount")
   fb_fans_count_em.insert(0, "{:,}".format(fb_fans_count))  
   #FbPeopleTalkingAboutYouCount
   fb_people_talking_about_you_count_em = soup.find(id = "FbPeopleTalkingAboutYouCount")
   fb_people_talking_about_you_count_em.insert(0, "{:,}".format(fb_talking_about_count))
   #FbTotalPostsCount
   fb_post_count_em = soup.find(id = "FbTotalPostsCount")
   fb_post_count_em.insert(0, "{:,}".format(fb_new_posts_count))
   
   #FbNewPhotoCount
   fb_new_photo_count_em = soup.find(id = "FbNewPhotoCount")
   fb_new_photo_count_em.insert(0, "{:,}".format(fb_new_photo_count))

   #FbNewLinkCount
   fb_new_link_count_em = soup.find(id = "FbNewLinkCount")
   fb_new_link_count_em.insert(0, "{:,}".format(fb_new_link_count))

   #FbNewVideoCount
   fb_new_video_count_em = soup.find(id = "FbNewVideoCount")
   fb_new_video_count_em.insert(0, "{:,}".format(fb_new_video_count))

   #FbNewStatusCount
   fb_new_status_count_em = soup.find(id = "FbNewStatusCount")
   fb_new_status_count_em.insert(0, "{:,}".format(fb_new_status_count))

   #FbAverageReactionsByPostCount
   fb_new_average_reactions_by_post_count_em = soup.find(id="FbAverageReactionsByPostCount")
   fb_new_average_reactions_by_post_count_em.insert(0, "{:,}".format(fb_new_average_reactions_by_post))
   #FbAverageSharesByPost
   fb_new_average_shares_by_post_em = soup.find(id="FbAverageSharesByPost")
   fb_new_average_shares_by_post_em.insert(0, "{:,}".format(fb_new_average_shares_by_post))
   #FbAverageCommentsByPost
   fb_new_average_comments_by_post_em = soup.find(id="FbAverageCommentsByPost")
   fb_new_average_comments_by_post_em.insert(0, "{:,}".format(fb_new_average_comments_by_post))
   #FbAverageLikesByPost
   fb_new_average_likes_by_post_em = soup.find(id="FbAverageLikesByPost")
   fb_new_average_likes_by_post_em.insert(0, "{:,}".format(fb_new_average_likes_by_post))
   #FbAverageLovesByPost
   fb_new_average_loves_by_post_em = soup.find(id="FbAverageLovesByPost")
   fb_new_average_loves_by_post_em.insert(0, "{:,}".format(fb_new_average_loves_by_post))
   #FbAverageWowsByPost
   fb_new_average_wows_by_post_em = soup.find(id="FbAverageWowsByPost")
   fb_new_average_wows_by_post_em.insert(0, "{:,}".format(fb_new_average_wows_by_post))
   #FbAverageHahasByPost
   fb_new_average_hahas_by_post_em = soup.find(id="FbAverageHahasByPost")
   fb_new_average_hahas_by_post_em.insert(0, "{:,}".format(fb_new_average_hahas_by_post))
   #FbAverageAngrysByPost
   fb_new_average_angrys_by_post_em = soup.find(id="FbAverageAngrysByPost")
   fb_new_average_angrys_by_post_em.insert(0, "{:,}".format(fb_new_average_angrys_by_post))
   #FbAverageSadsByPost
   fb_new_average_sads_by_post_em = soup.find(id="FbAverageSadsByPost")
   fb_new_average_sads_by_post_em.insert(0, "{:,}".format(fb_new_average_sads_by_post))
   
   #TwTotalFollowersCount
   tw_total_followers_count_em = soup.find(id="TwTotalFollowersCount")
   tw_total_followers_count_em.insert(0, "{:,}".format(tw_followers_count))
   
   #TwTotalTweetsCount
   tw_total_tweets_count_em = soup.find(id="TwTotalTweetsCount")
   tw_total_tweets_count_em.insert(0, "{:,}".format(tw_new_tweets_count))

   #TwTotalRetweetsCount-> Of total user's posts, x are retweets
   tw_total_retweets_count_em = soup.find(id="TwTotalRetweetsCount")
   tw_total_retweets_count_em.insert(0, "{:,}".format(tw_new_retweets_count ))

   #TwTotalUserTweetsCount
   tw_total_user_tweets_count_em  = soup.find(id="TwTotalUserTweetsCount")
   tw_total_user_tweets_count_em.insert(0, "{:,}".format( tw_new_tweets_count))

   #TwAverageRetweetsByTweetCount
   tw_average_retweets_by_tweet_count_em = soup.find(id="TwAverageRetweetsByTweetCount")
   tw_average_retweets_by_tweet_count_em.insert(0, "{:,}".format(tw_new_average_retweets_count))
   
   #TwAverageFavoritesByTweetCount
   tw_average_favorites_by_tweet_count_em = soup.find(id="TwAverageFavoritesByTweetCount")
   tw_average_favorites_by_tweet_count_em.insert(0, "{:,}".format(tw_new_average_favorites_count))
   """
     ->Reports
   """
   py.sign_in('felipeguerrabr', 'iouygb9exa')
   
   #Distribution of posts and reactions(positive/negative)
   positive_reputation = (fb_new_average_likes_by_post + fb_new_average_loves_by_post + fb_new_average_hahas_by_post + fb_new_average_wows_by_post) / fb_new_average_reactions_by_post
   positive_reputation *= 100   
   negative_reputation = (fb_new_average_angrys_by_post + fb_new_average_sads_by_post) / fb_new_average_reactions_by_post
   negative_reputation *= 100
   fig = {
       'data' : [
           {
               'labels' : ["Status", "Links", "Videos", "Fotos"],
               'values' : [fb_new_percentage_status, fb_new_percentage_links, fb_new_percentage_videos, fb_new_percentage_photos], 
               'type' : 'pie',
                 'marker': {'colors': ['rgb(177, 180, 34)', 'rgb(206, 206, 40)', 'rgb(175, 51, 21)', 'rgb(35, 36, 21)']},
               'domain': {'x': [0, .48], 'y': [0, .49]}
           },
           {
             'labels' : ["Positivas", "Negativas"],
             'values' : [positive_reputation, negative_reputation],
             'type' : 'pie',
             'marker' : {'colors' : ['rgb(0, 255, 0)', 'rgb(255, 0, 0)']},
             'domain': {'x': [.52, 1], 'y': [0, .49]}
           }
       ],
       'layout': {'title': "Distribuicao dos posts e reacoes", 'showlegend': True, "width" : 300, "height" : 300}
   }
   py.image.save_as(fig, filename = "html_pages/plots/" + nickname + '_fb_pie_charts1.png') 
   new_tag = soup.new_tag('img', src = '../plots/' + nickname + "_fb_pie_charts1.png")
   plot_div_out = soup.find(id="PostPieChartesOut")
   plot_div_out.insert(0, new_tag )
   
   #Popularity by type of post
   datan = [go.Bar(
            x = fb_new_status_type_popularity_dic.keys() ,
            y = fb_new_status_type_popularity_dic.values()
   )]
   layoutn = go.Layout(
    title='Popularidade por tipo de post',
   )
   fign = go.Figure(data = datan, layout = layoutn)
   py.image.save_as(fign, filename = "html_pages/plots/" + nickname + '_fb_bar_chart_pop.png') 
   new_tag = soup.new_tag('img', src = '../plots/' + nickname + '_fb_bar_chart_pop.png')
   plot_div_out = soup.find(id="FbPostByPop")
   plot_div_out.insert(0, new_tag )
   
   #FbMostPostedType
   percentage_list = [fb_new_percentage_status, fb_new_percentage_links, fb_new_percentage_videos, fb_new_percentage_photos]
   max_percentage_item = max(percentage_list)
   max_percentage_index = percentage_list.index(max_percentage_item)
   if len([k for k in percentage_list if k == max_percentage_item]) == 1:
       if max_percentage_index == 0:
           fb_most_posted_typed_em = soup.find(id="FbMostPostedType")
           fb_most_posted_typed_em.insert(0, "status")
           fb_most_posted_type = "status"
       elif max_percentage_index == 1:
           fb_most_posted_typed_em = soup.find(id="FbMostPostedType")
           fb_most_posted_typed_em.insert(0, "links")
           fb_most_posted_type = "links"          
       elif max_percentage_index == 2:
           fb_most_posted_typed_em = soup.find(id="FbMostPostedType")
           fb_most_posted_typed_em.insert(0, "videos")
           fb_most_posted_type = "videos"   
       else:
           fb_most_posted_typed_em = soup.find(id="FbMostPostedType")
           fb_most_posted_typed_em.insert(0, "fotos")
           fb_most_posted_type = "fotos"
   else:
           fb_most_posted_typed_em = soup.find(id="FbMostPostedType")
           fb_most_posted_typed_em.insert(0, "todos igualmente")
           fb_most_posted_type = "todos igualmente"
   #FbMostImportantReactionType -> positiva/negative
   fb_most_important_reaction_type_em = soup.find(id="FbMostImportantReactionType")
   if positive_reputation > negative_reputation:
       fb_overall_reaction = "positiva"
       fb_most_important_reaction_type_em.insert(0, "positiva")
   elif positive_reputation < negative_reputation:
       fb_overall_reaction = "negativa"
       fb_most_important_reaction_type_em.insert(0, "negativa")
   else:
       fb_overall_reaction = "neutra"
       fb_most_important_reaction_type_em.insert(0, "neutra")
   
   #FbMostImportantPostTypeForPopularity
   fb_new_status_type_most_popular = max(fb_new_status_type_popularity_dic, key = fb_new_status_type_popularity_dic.get)   
   fb_most_important_post_type_for_popularity = utilities.translate_post_type(fb_new_status_type_most_popular)
   fb_most_important_post_type_for_popularity_em = soup.find(id="FbMostImportantPostTypeForPopularity")
   fb_most_important_post_type_for_popularity_em.insert(0, utilities.translate_post_type(fb_new_status_type_most_popular))

   #FbMostImportantPostTypeForReaction 
   fb_most_important_post_type_for_reaction_em = soup.find(id="FbMostImportantPostTypeForReaction")
   positive_reactions_by_post_type_dic = {"photo" : fb_new_hahas_distribution["photo"] + fb_new_wows_distribution["photo"] + fb_new_likes_distribution["photo"] + fb_new_loves_distribution["photo"] , "link" : fb_new_hahas_distribution["link"] + fb_new_wows_distribution["link"] + fb_new_likes_distribution["link"] + fb_new_loves_distribution["link"] , "video" : fb_new_hahas_distribution["video"] + fb_new_wows_distribution["video"] + fb_new_likes_distribution["video"] + fb_new_loves_distribution["video"], "status" : fb_new_hahas_distribution["status"] + fb_new_wows_distribution["status"] + fb_new_likes_distribution["status"] + fb_new_loves_distribution["status"], "note" : fb_new_hahas_distribution["note"] + fb_new_wows_distribution["note"] + fb_new_likes_distribution["note"] + fb_new_loves_distribution["note"]}
   negative_reactions_by_post_type_dic = {"photo" : fb_new_sads_distribution["photo"] + fb_new_angrys_distribution["photo"] , "link" : fb_new_sads_distribution["link"] + fb_new_angrys_distribution["link"], "video" : fb_new_sads_distribution["video"] + fb_new_angrys_distribution["video"], "status" : fb_new_sads_distribution["status"] + fb_new_angrys_distribution["status"] , "note" : fb_new_sads_distribution["note"] + fb_new_angrys_distribution["note"] }
   
   #FbPostReactionByPostType
   trace1 = go.Bar(
       x = positive_reactions_by_post_type_dic.keys(),
       y = positive_reactions_by_post_type_dic.values(),
       name='Positiva'
   )
    
   trace2 = go.Bar(
      x = negative_reactions_by_post_type_dic.keys() ,
      y = negative_reactions_by_post_type_dic.values(),
      name='Negativa'
   )

   data = [trace1, trace2]
   layout = go.Layout(
      barmode='stack',
      title='Tipo de reacao por tipo de post'
   )
   fig = go.Figure(data = data, layout = layout)
   py.image.save_as(fig, filename = "html_pages/plots/" + nickname + '_fb_bar_chart_reaction_by_post_type.png')
   new_tag = soup.new_tag('img', src = '../plots/' + nickname + '_fb_bar_chart_reaction_by_post_type.png')
   plot_div_out = soup.find(id="FbPostReactionByPostType")
   plot_div_out.insert(0, new_tag)

   if positive_reputation > negative_reputation:
     #max(likes + loves + wows + hahas) ? post type      
     fb_most_important_post_type_for_positive_reaction = max(positive_reactions_by_post_type_dic, key = positive_reactions_by_post_type_dic.get)
     fb_most_important_post_type_for_reaction_em.insert(0, utilities.translate_post_type(fb_most_important_post_type_for_positive_reaction))
   elif positive_reputation < negative_reputation:
     #max(sads + angrys) ? post type
     fb_most_important_post_type_for_negative_reaction = max(negative_reactions_by_post_type_dic, key = negative_reactions_by_post_type_dic.get)
     fb_most_important_post_type_for_reaction_em.insert(0, utilities.translate_post_type(fb_most_important_post_type_for_negative_reaction))
   else:
     fb_most_important_post_type_for_reaction_em.insert(0, "todos, pois a reacao foi neutra")
   
   #Most popular post
   # Sort by myfield (ascending value) and return first document
   most_popular_post = db["facebook_posts"].find({"user_nickname" : nickname}, sort=[("status_num_reactions", -1)]).limit(1)
   for m in most_popular_post:
       #FbPostType
       fb_post_type_em = soup.find(id="FbPostType")
       fb_post_type_em.insert(0, utilities.translate_post_type(m["status_type"]).capitalize())
       #FbPostStatusMessage 
       fb_post_status_message_em = soup.find(id="FbPostStatusMessage")
       fb_post_status_message_em.insert(0, m["status_message"])
       #FbPostStatusNumReactions
       fb_post_status_num_reactions_em = soup.find(id="FbPostStatusNumReactions")
       fb_post_status_num_reactions_em.insert(0, "{:,}".format(m["status_num_reactions"]))
       #FbPostStatusNumComments
       fb_post_status_num_comments_em = soup.find(id="FbPostStatusNumComments")
       fb_post_status_num_comments_em.insert(0, "{:,}".format(m["status_num_comments"]))
       #FbPostStatusNumShares
       fb_post_status_num_shares_em = soup.find(id="FbPostStatusNumShares")
       fb_post_status_num_shares_em.insert(0, "{:,}".format(m["status_num_shares"]))

   #Distribution of positive/negative comments
   fig2 = {
       'data' : [
           {
              'labels' : ["Positivos", "Negativos"],
              'values' : [fb_new_average_negative_comments_by_post, fb_new_average_positive_comments_by_post],
              'type' : 'pie',
              'marker' : {'colors' : ['rgb(0, 255, 0)', 'rgb(255, 0, 0)']},
           }
       ],
       'layout': {'title': "Distribuicao dos comentarios", 'showlegend': True, "width" : 300, "height" : 300}
   }
   py.image.save_as(fig2, filename = "html_pages/plots/" + nickname + '_fb_pie_charts2.png') 
   new_tag = soup.new_tag('img', src = '../plots/' + nickname + "_fb_pie_charts2.png")
   plot_div_out = soup.find(id="CommentsPieChartesOut")
   plot_div_out.insert(0, new_tag )
   
   #FbMostImportantCommentReaction
   fb_most_important_comment_reaction_em = soup.find(id="FbMostImportantCommentReaction")
   if fb_new_average_negative_comments_by_post > fb_new_average_positive_comments_by_post:
       fb_most_important_comment_reaction_em.insert(0, "negativa")           
   elif fb_new_average_negative_comments_by_post < fb_new_average_positive_comments_by_post:
       fb_most_important_comment_reaction_em.insert(0, "positiva")
   else:
       fb_most_important_comment_reaction_em.insert(0, "neutra")

   #Qual tipo de post trouxe mais comentarios positivos/negativos?
   #Qual tipo de post trouxe mais reacoes negativas/positivas

   #Fans by country. table -> Pais, Lingua e numro de fas
   table_data = [['Pais', 'Fas']]
   for k, v in fb_fans_country_distribution_dic.iteritems():
       #get country fullname and language by the abreviation
       country_name = utilities.get_country_name(k)
       country_name = ' '.join([w.capitalize() for w in country_name.split()])
       country_name += " (" + k + ")" 
       v = "{:,}".format(v)
       table_data.append([country_name,v])

   figure = FF.create_table(table_data, height_constant=120)
   countries = [k for k in fb_fans_country_distribution_dic.keys()]
   fans = [v for v in fb_fans_country_distribution_dic.values()]
   trace1 = go.Bar(x=countries, y=fans, xaxis='x2', yaxis='y2',
                marker=dict(color='#0099ff'),
                name='Fas por pais')
   figure['data'].extend(go.Data([trace1]))
   figure.layout.yaxis.update({'domain': [0, .45]})
   figure.layout.yaxis2.update({'domain': [.6, 1]})
   figure.layout.yaxis2.update({'anchor': 'x2'})
   figure.layout.xaxis2.update({'anchor': 'y2'})
   figure.layout.yaxis2.update({'title': 'Fas'}) 
   figure.layout.margin.update({'t':75, 'l':50})
   figure.layout.update({'title': 'Fas por pais'})
   figure.layout.update({'height':1600})
   py.image.save_as(figure, filename = "html_pages/plots/" + nickname + '_fb_fans_country_table.png') 
   new_tag = soup.new_tag('img', src = '../plots/' + nickname + '_fb_fans_country_table.png')
   plot_div_out = soup.find(id="FbFansByCountry")
   plot_div_out.insert(0, new_tag )
   
   #FbMostFansCountryName
   most_fans_country = max(fb_fans_country_distribution_dic, key = fb_fans_country_distribution_dic.get)
   
   fb_most_fans_country_name_em = soup.find(id="FbMostFansCountryName")
   most_fans_country = utilities.get_country_name(most_fans_country)
   most_fans_country = ' '.join([w.capitalize() for w in most_fans_country.split()])
   fb_most_fans_country = most_fans_country
   fb_most_fans_country_name_em.insert(0, most_fans_country)

   #FbLessFansCountryName
   less_fans_country = min(fb_fans_country_distribution_dic, key = fb_fans_country_distribution_dic.get)
   fb_less_fans_country_em = soup.find(id="FbLessFansCountryName")
   less_fans_country = utilities.get_country_name(less_fans_country)
   less_fans_country = ' '.join([w.capitalize() for w in less_fans_country.split()])
   fb_less_fans_country = less_fans_country
   fb_less_fans_country_em.insert(0, less_fans_country)

   #Word cloud of comments -> WordCloudComments
   wordcloud = WordCloud(max_font_size=40, relative_scaling=.5).generate(raw_comments)
   plt.figure()
   plt.imshow(wordcloud)
   plt.axis("off")
   plt.savefig("html_pages/plots/" + nickname + "_wordscloud.png", dpi = 55)
   
   new_tag = soup.new_tag('img', src='../plots/' + nickname + "_wordscloud.png")
   plot_div_out = soup.find(id="WordCloudComments")
   plot_div_out.insert(0, new_tag)
    
   #Distribution of tweets -> Retweets and Tweets (TwPostsDistribution)
   fig3 = {
      'data' : [
           {
              'labels' : ["Escritos por voce", "Retweets"],
              'values' : [tw_new_tweets_count - tw_new_retweets_count, tw_new_retweets_count],
              'type' : 'pie'
           }
      ],
       'layout': {'title': "Distribuicao dos posts no twitter", 'showlegend': True, "width" : 400, "height" : 400}
   }
   py.image.save_as(fig3, filename = "html_pages/plots/" + nickname + '_tw_pie_charts1.png') 
   new_tag = soup.new_tag('img', src = '../plots/' + nickname + "_tw_pie_charts1.png")
   plot_div_out = soup.find(id="TwPostsDistribution")
   plot_div_out.insert(0, new_tag )
    
   #Word cloud of your retweets -> WordCloudRetweets
   wordcloud = WordCloud(max_font_size=40, relative_scaling=.5).generate(raw_retweets)
   plt.figure()
   plt.imshow(wordcloud)
   plt.axis("off")
   plt.savefig("html_pages/plots/" + nickname + "_wordscloud2.png", dpi = 55)
   new_tag = soup.new_tag('img', src='../plots/' + nickname + "_wordscloud2.png")
   plot_div_out = soup.find(id="WordCloudRetweets")
   plot_div_out.insert(0, new_tag)

   #most popular tweet
   most_popular_post = db["twitter_posts"].find({"user_nickname" : nickname}, sort=[("favorite_count", -1)]).limit(1)
   for m in most_popular_post:
       #TwTweetText 
       tw_tweet_text_em = soup.find(id="TwTweetText")
       tw_tweet_text_em.insert(0, m["text"])
       
       #TwTweetNumFavorites
       tw_tweet_num_favorites_em = soup.find(id="TwTweetNumFavorites")
       tw_tweet_num_favorites_em.insert(0, "{:,}".format(m["favorite_count"]))
       
       #TwTweetNumRetweets
       tw_tweet_num_retweets_em = soup.find(id="TwTweetNumRetweets")
       tw_tweet_num_retweets_em.insert(0, "{:,}".format(m["retweet_count"]))
        
   file1 = open("html_pages/temporary/overview_" + nickname + ".html", "wb")
   file1.write(soup.prettify())
   file1.close()

def scrap_posts(db, nickname):
   file1 = open("html_pages/total/posts.html", "rb")
   html = file1.read()
   file1.close()
   soup = bs(html, "lxml")

   #top 10 general of posts (order by number of comments, number of reactions and number of shares)
   top_10_popular_posts = db["facebook_posts"].find({"user_nickname" : nickname}, sort=[("status_num_reactions", -1), ("status_num_comments", -1), ("status_num_shares", -1)] ).limit(10)   
   count = 1
   for t in top_10_popular_posts:
       #count-postMessage
       fb_post_status_message_em = soup.find(id= "{}-postMessage".format(str(count)))
       fb_post_status_message_em.insert(0, t["status_message"])
       #count-PostStatusNumReactions
       fb_post_status_num_reactions_em = soup.find(id="{}-PostStatusNumReactions".format(str(count)))
       fb_post_status_num_reactions_em.insert(0, "{:,}".format(t["status_num_reactions"]))
       #count-PostStatusNumComments
       fb_post_status_num_comments_em = soup.find(id="{}-PostStatusNumComments".format(str(count)))
       fb_post_status_num_comments_em.insert(0, "{:,}".format(t["status_num_comments"]))
       #count-PostStatusNumShares
       fb_post_status_num_shares_em = soup.find(id="{}-PostStatusNumShares".format(str(count)))
       fb_post_status_num_shares_em.insert(0, "{:,}".format(t["status_num_shares"]))

       count += 1

   #most hated post
   most_hated_post = db["facebook_posts"].find({"user_nickname" : nickname}, sort=[("status_num_angrys", -1), ("status_num_sads", -1)]).limit(1)
   for m in most_hated_post:
       #mosthated-postMessage
       fb_post_status_message_em = soup.find(id="mosthated-postMessage")
       fb_post_status_message_em.insert(0, m["status_message"])
       #mosthated-PostStatusNumReactions
       fb_post_status_num_reactions_em = soup.find(id="mosthated-PostStatusNumReactions")
       fb_post_status_num_reactions_em.insert(0, "{:,}".format(m["status_num_reactions"]))
       #mosthated-numAngrys
       fb_post_most_hated_num_angrys_em = soup.find(id="mosthated-numAngrys")
       fb_post_most_hated_num_angrys_em.insert(0, "{:,}".format(m["status_num_angrys"]))
       #mosthated-numSads
       fb_post_most_hated_num_sads_em = soup.find(id="mosthated-numSads")
       fb_post_most_hated_num_sads_em.insert(0, "{:,}".format(m["status_num_sads"]))
       #mosthated-PostStatusNumComments
       fb_post_status_num_comments_em = soup.find(id="mosthated-PostStatusNumComments" )
       fb_post_status_num_comments_em.insert(0, "{:,}".format(m["status_num_comments"]))
       #mosthated-PostStatusNumShares
       fb_post_status_num_shares_em = soup.find(id="mosthated-PostStatusNumShares")
       fb_post_status_num_shares_em.insert(0, "{:,}".format(m["status_num_shares"]))

   #most loved post
   most_loved_post = db["facebook_posts"].find({"user_nickname" : nickname}, sort=[("status_num_likes", -1), ("status_num_loves", -1)]).limit(1)
   for m in most_loved_post:
       #mosthated-postMessage
       fb_post_status_message_em = soup.find(id="mostloved-postMessage")
       fb_post_status_message_em.insert(0, m["status_message"])
       #mosthated-PostStatusNumReactions
       fb_post_status_num_reactions_em = soup.find(id="mostloved-PostStatusNumReactions")
       fb_post_status_num_reactions_em.insert(0, "{:,}".format(m["status_num_reactions"]))

       #mostloved-numLikes
       fb_most_loved_num_likes_em = soup.find(id = "mostloved-numLikes")
       fb_most_loved_num_likes_em.insert(0, "{:,}".format(m["status_num_likes"]))
  
       #mostloved-numLoves
       fb_most_loved_num_loves_em = soup.find(id = "mostloved-numLoves")
       fb_most_loved_num_loves_em.insert(0, "{:,}".format( m["status_num_loves"]))

       #mosthated-PostStatusNumComments
       fb_post_status_num_comments_em = soup.find(id="mostloved-PostStatusNumComments" )
       fb_post_status_num_comments_em.insert(0, "{:,}".format(m["status_num_comments"]))
       #mosthated-PostStatusNumShares
       fb_post_status_num_shares_em = soup.find(id="mostloved-PostStatusNumShares")
       fb_post_status_num_shares_em.insert(0, "{:,}".format(m["status_num_shares"]))

   #max loves post
   max_loves_post = db["facebook_posts"].find({"user_nickname" : nickname}, sort=[("status_num_loves",-1)]).limit(1)
   for m in max_loves_post:
       #maxloves-postMessage
       fb_post_status_message_em = soup.find(id="maxloves-postMessage")
       fb_post_status_message_em.insert(0, m["status_message"])
       #maxloves-PostStatusNumReactions
       fb_post_status_num_reactions_em = soup.find(id="maxloves-PostStatusNumReactions")
       fb_post_status_num_reactions_em.insert(0, "{:,}".format(m["status_num_loves"]))
       #maxloves-PostStatusNumComments
       fb_post_status_num_comments_em = soup.find(id="maxloves-PostStatusNumComments")
       fb_post_status_num_comments_em.insert(0, "{:,}".format(m["status_num_comments"]))
       #maxtloves-PostStatusNumShares
       fb_post_status_num_shares_em = soup.find(id="maxloves-PostStatusNumShares")
       fb_post_status_num_shares_em.insert(0, "{:,}".format(m["status_num_shares"])) 
   
   #max likes post
   max_likes_post = db["facebook_posts"].find({"user_nickname" : nickname}, sort=[("status_num_likes",-1)]).limit(1)
   for m in max_likes_post:
       #maxlikes-postMessage
       fb_post_status_message_em = soup.find(id="maxlikes-postMessage")
       fb_post_status_message_em.insert(0, m["status_message"])
       #maxlikes-PostStatusNumReactions
       fb_post_status_num_reactions_em = soup.find(id="maxlikes-PostStatusNumReactions")
       fb_post_status_num_reactions_em.insert(0, "{:,}".format(m["status_num_likes"]))
       #maxlikes-PostStatusNumComments
       fb_post_status_num_comments_em = soup.find(id="maxlikes-PostStatusNumComments" )
       fb_post_status_num_comments_em.insert(0, "{:,}".format(m["status_num_comments"]))
       #maxtlikes-PostStatusNumShares
       fb_post_status_num_shares_em = soup.find(id="maxlikes-PostStatusNumShares")
       fb_post_status_num_shares_em.insert(0, "{:,}".format(m["status_num_shares"])) 

   #max wows post
   max_wows_post = db["facebook_posts"].find({"user_nickname" : nickname}, sort=[("status_num_wows",-1)]).limit(1)
   for m in max_wows_post:
       #maxwows-postMessage
       fb_post_status_message_em = soup.find(id="maxwows-postMessage")
       fb_post_status_message_em.insert(0, m["status_message"])
       #maxwows-PostStatusNumReactions
       fb_post_status_num_reactions_em = soup.find(id="maxwows-PostStatusNumReactions")
       fb_post_status_num_reactions_em.insert(0, "{:,}".format(m["status_num_wows"]))
       #maxwows-PostStatusNumComments
       fb_post_status_num_comments_em = soup.find(id="maxwows-PostStatusNumComments" )
       fb_post_status_num_comments_em.insert(0, "{:,}".format(m["status_num_comments"]))
       #maxwows-PostStatusNumShares
       fb_post_status_num_shares_em = soup.find(id="maxwows-PostStatusNumShares")
       fb_post_status_num_shares_em.insert(0, "{:,}".format(m["status_num_shares"])) 

   #max hahas post
   max_hahas_post = db["facebook_posts"].find({"user_nickname" : nickname}, sort=[("status_num_hahas",-1)]).limit(1)
   for m in max_hahas_post:
       #maxhahas-postMessage
       fb_post_status_message_em = soup.find(id="maxhahas-postMessage")
       fb_post_status_message_em.insert(0, m["status_message"])
       #maxhahas-PostStatusNumReactions
       fb_post_status_num_reactions_em = soup.find(id="maxhahas-PostStatusNumReactions")
       fb_post_status_num_reactions_em.insert(0, "{:,}".format(m["status_num_hahas"]))
       #maxhahas-PostStatusNumComments
       fb_post_status_num_comments_em = soup.find(id="maxhahas-PostStatusNumComments" )
       fb_post_status_num_comments_em.insert(0, "{:,}".format(m["status_num_comments"]))
       #maxhahas-PostStatusNumShares
       fb_post_status_num_shares_em = soup.find(id="maxhahas-PostStatusNumShares")
       fb_post_status_num_shares_em.insert(0, "{:,}".format(m["status_num_shares"])) 

   #max angrys post
   max_angrys_post = db["facebook_posts"].find({"user_nickname" : nickname}, sort=[("status_num_angrys",-1)]).limit(1)
   for m in max_angrys_post:
       #maxangrys-postMessage
       fb_post_status_message_em = soup.find(id="maxangrys-postMessage")
       fb_post_status_message_em.insert(0, m["status_message"])
       #maxangrys-PostStatusNumReactions
       fb_post_status_num_reactions_em = soup.find(id="maxangrys-PostStatusNumReactions")
       fb_post_status_num_reactions_em.insert(0, "{:,}".format(m["status_num_angrys"]))
       #maxangrys-PostStatusNumComments
       fb_post_status_num_comments_em = soup.find(id="maxangrys-PostStatusNumComments" )
       fb_post_status_num_comments_em.insert(0, "{:,}".format(m["status_num_comments"]))
       #maxangrys-PostStatusNumShares
       fb_post_status_num_shares_em = soup.find(id="maxangrys-PostStatusNumShares")
       fb_post_status_num_shares_em.insert(0, "{:,}".format(m["status_num_shares"])) 

   #max sads post
   max_sads_post = db["facebook_posts"].find({"user_nickname" : nickname}, sort=[("status_num_angrys",-1)]).limit(1)
   for m in max_sads_post:
       #maxsads-postMessage
       fb_post_status_message_em = soup.find(id="maxsads-postMessage")
       fb_post_status_message_em.insert(0, m["status_message"])
       #maxsads-PostStatusNumReactions
       fb_post_status_num_reactions_em = soup.find(id="maxsads-PostStatusNumReactions")
       fb_post_status_num_reactions_em.insert(0, "{:,}".format(m["status_num_sads"]))
       #maxsads-PostStatusNumComments
       fb_post_status_num_comments_em = soup.find(id="maxsads-PostStatusNumComments" )
       fb_post_status_num_comments_em.insert(0, "{:,}".format(m["status_num_comments"]))
       #maxsads-PostStatusNumShares
       fb_post_status_num_shares_em = soup.find(id="maxsads-PostStatusNumShares")
       fb_post_status_num_shares_em.insert(0, "{:,}".format(m["status_num_shares"])) 
   
   #Hashtags cloud -> "status_hashtags" : [ "#DesafioCelebridades" ]
   posts = db["facebook_posts"].find({"user_nickname" : nickname})
   raw_hashtags = ""
   for p in posts:
       status_hashtags = p["status_hashtags"]
       raw_hashtags += " ".join(status_hashtags)
   wordcloud = WordCloud(max_font_size = 40, relative_scaling = .5).generate(raw_hashtags)
   plt.figure()
   plt.imshow(wordcloud)
   plt.axis("off")
   plt.savefig("html_pages/plots/" + nickname + "_wordscloudhash.png", dpi = 55)
   new_tag = soup.new_tag('img', src='../plots/' + nickname + "_wordscloudhash.png")
   plot_div_out = soup.find(id="WordCloudHashTags")
   plot_div_out.insert(0, new_tag)

   """
   What is the best day of the week to post?
   """

   #PostDistributionByYear
   data = [go.Bar(
            x = [a.keys()[0] for a in fb_years_list],
            y = [a.values()[0] for a in fb_years_list],
            name = "Ano"
   )]
   py.image.save_as(data, filename = "html_pages/plots/" + nickname + '_fb_bar_chart_years.png') 
   new_tag = soup.new_tag('img', src = '../plots/' + nickname + '_fb_bar_chart_years.png')
   plot_div_out = soup.find(id = "PostDistributionByYear")
   plot_div_out.insert(0, new_tag)
   #PostDistributionByMonth
   data = [go.Bar(
            x = [a.keys()[0] for a in fb_months_list],
            y = [a.values()[0] for a in fb_months_list],
            name = "Mes"
   )]
   py.image.save_as(data, filename = "html_pages/plots/" + nickname + '_fb_bar_chart_months.png') 
   new_tag = soup.new_tag('img', src = '../plots/' + nickname + '_fb_bar_chart_months.png')
   plot_div_out = soup.find(id = "PostDistributionByMonth")
   plot_div_out.insert(0, new_tag)
   #PostDistributionByDayOfWeek   
   #days_of_week_dic
   trace1 = go.Bar(
            x = [a.keys()[0] for a in fb_days_of_week_list],
            y = [a.values()[0] for a in fb_days_of_week_list],
            name = "Numero de posts"
   )

   trace2 = go.Bar(
            x = [a.keys()[0] for a in fb_days_of_week_popularity_list],
            y = [a.values()[0] for a in fb_days_of_week_popularity_list],
            name = "Popularidade"
   )
   data = [trace1, trace2]
   layout = go.Layout(
       barmode='stack'
   )
   fig10 = go.Figure(data = data, layout = layout)
   py.image.save_as(fig10, filename = "html_pages/plots/" + nickname + '_fb_bar_chart_days_of_week.png') 
   new_tag = soup.new_tag('img', src = '../plots/' + nickname + '_fb_bar_chart_days_of_week.png')
   plot_div_out = soup.find(id = "PostDistributionByDayOfWeek")
   plot_div_out.insert(0, new_tag)   
   #MostPostsDayOfWeek
   fb_most_posts_day_of_week_em = soup.find(id="MostPostsDayOfWeek")
   fb_most_posts_day_of_week_em.insert(0, fb_most_posts_day_of_week)
   #MostPopularDayOfWeek
   fb_most_popular_day_of_week_em = soup.find(id="MostPopularDayOfWeek")
   fb_most_popular_day_of_week_em.insert(0, fb_most_popular_day_of_week)
  
   #Tweets 
   #top 10 general of tweets (order by number of favorites and number of retweets)
   top_10_popular_tweets = db["twitter_posts"].find({"user_nickname" : nickname}, sort=[("favorite_count", -1), ("retweets_count", -1)]).limit(10)   
   count = 1
   for t in top_10_popular_tweets:
       #count-twMessage
       tw_post_status_message_em = soup.find(id= "{}-twMessage".format(str(count)))
       tw_post_status_message_em.insert(0, t["text"])
       #count-twStatusNumReactions
       tw_post_status_num_reactions_em = soup.find(id="{}-twStatusNumReactions".format(str(count)))
       tw_post_status_num_reactions_em.insert(0, "{:,}".format(t["favorite_count"]))
       #count-twStatusNumComments
       tw_post_status_num_comments_em = soup.find(id="{}-twStatusNumComments".format(str(count)))
       tw_post_status_num_comments_em.insert(0, "{:,}".format(len(t["retweets_list"])))
       count += 1

   #max favorites
   max_favorites_post = db["twitter_posts"].find({"user_nickname" : nickname}, sort=[("favorite_count", -1)]).limit(1)
   for m in max_favorites_post:
       #maxfavorites-twMessage
       fb_post_status_message_em = soup.find(id="maxfavorites-twMessage")
       fb_post_status_message_em.insert(0, m["text"])
       #maxfavorites-twStatusNumFavorites
       fb_post_status_num_reactions_em = soup.find(id="maxfavorites-twStatusNumFavorites")
       fb_post_status_num_reactions_em.insert(0, "{:,}".format(m["favorite_count"]))
       #maxfavorites-twStatusNumRetweets
       fb_post_most_hated_num_angrys_em = soup.find(id="maxfavorites-twStatusNumRetweets")
       fb_post_most_hated_num_angrys_em.insert(0, "{:,}".format( len(m["retweets_list"])))

   #max retweets
   max_retweets_post = db["twitter_posts"].find({"user_nickname" : nickname}, sort=[("retweets_count", -1)]).limit(1)
   for m in max_retweets_post:
       #maxretweets-twMessage
       fb_post_status_message_em = soup.find(id="maxretweets-twMessage")
       fb_post_status_message_em.insert(0, m["text"])
       #maxretweets-twStatusNumFavorites
       fb_post_status_num_reactions_em = soup.find(id="maxretweets-twStatusNumFavorites")
       fb_post_status_num_reactions_em.insert(0, "{:,}".format(m["favorite_count"]))
       #maxretweets-twStatusNumRetweets
       fb_post_most_hated_num_angrys_em = soup.find(id="maxretweets-twStatusNumRetweets")
       fb_post_most_hated_num_angrys_em.insert(0, "{:,}".format(len(m["retweets_list"])))

 
   #TwDistributionByYear
   data = [go.Bar(
            x = [a.keys()[0] for a in tw_years_list],
            y = [a.values()[0] for a in tw_years_list],
            name = "Ano"
   )]
   py.image.save_as(data, filename = "html_pages/plots/" + nickname + '_tw_bar_chart_years.png') 
   new_tag = soup.new_tag('img', src = '../plots/' + nickname + '_tw_bar_chart_years.png')
   plot_div_out = soup.find(id = "TwDistributionByYear")
   plot_div_out.insert(0, new_tag)
   

   #TwDistributionByMonth
   data = [go.Bar(
            x = [a.keys()[0] for a in tw_months_list],
            y = [a.values()[0] for a in tw_months_list],
            name = "Mes"
   )]
   py.image.save_as(data, filename = "html_pages/plots/" + nickname + '_tw_bar_chart_months.png') 
   new_tag = soup.new_tag('img', src = '../plots/' + nickname + '_tw_bar_chart_months.png')
   plot_div_out = soup.find(id = "TwDistributionByMonth")
   plot_div_out.insert(0, new_tag)
   
   #TwDistributionByDayOfWeek   
   #days_of_week_dic
  
   trace1 = go.Bar(
            x = [a.keys()[0] for a in tw_days_of_week_list],
            y = [a.values()[0] for a in tw_days_of_week_list],
            name = "Numero de posts"
   )

   trace2 = go.Bar(
            x = [a.keys()[0] for a in tw_days_of_week_popularity_list],
            y = [a.values()[0] for a in tw_days_of_week_popularity_list],
            name = "Popularidade"
   )
   data = [trace1, trace2]
   layout = go.Layout(
       barmode='stack'
   )
   fig10 = go.Figure(data=data, layout=layout)
   py.image.save_as(fig10, filename = "html_pages/plots/" + nickname + '_tw_bar_chart_days_of_week.png') 
   new_tag = soup.new_tag('img', src = '../plots/' + nickname + '_tw_bar_chart_days_of_week.png')
   plot_div_out = soup.find(id = "TwDistributionByDayOfWeek")
   plot_div_out.insert(0, new_tag)   
   
   #MostTwDayOfWeek
   tw_most_posts_day_of_week_em = soup.find(id="MostTwDayOfWeek")
   tw_most_posts_day_of_week_em.insert(0, tw_most_posts_day_of_week)
   
   #TwMostPopularDayOfWeek
   tw_most_popular_day_of_week_em = soup.find(id="TwMostPopularDayOfWeek")
   tw_most_popular_day_of_week_em.insert(0, tw_most_popular_day_of_week)

   file1 = open("html_pages/temporary/posts_" + nickname + ".html", "wb")
   file1.write(soup.prettify())
   file1.close()

def scrap_comments(db, nickname):
   file1 = open("html_pages/total/comments.html", "rb")
   html = file1.read()
   file1.close()
   soup = bs(html, "lxml")  
   
   #top 10 comments (number of likes)   
   #comments : [{"from" : { "name" : "Jessica Santos", "id" : "1001866373257244" }, "created_hour" : "16:08:02", "like_count" : 1, "created_date" : "2016-09-14", "created_time" : "2016-09-14T16:08:02+0000", "message" : "Marcelo Petrin", "id" : "10154665127642573_10154667165747573" }]
   posts = db["facebook_posts"].find({"user_nickname" : nickname})
   comments = []
   for p in posts:
       comments += p["comments"]   
   comments = [c for c in comments if c["message"]]
   top_10_posts = sorted(comments, key = lambda k : k["like_count"], reverse = True)[0:10]
   count = 1
   for t in top_10_posts:
       #count-postMessage
       fb_post_status_message_em = soup.find(id = str(count) + "-postMessage")
       fb_post_status_message_em.insert(0, str(t["message"]))
       #count-PostStatusNumLikes
       fb_post_status_num_likes_em = soup.find(id = str(count) + "-PostStatusNumLikes")
       fb_post_status_num_likes_em.insert(0, "{:,}".format(t["like_count"]))
       count += 1

   #top 10 retweets (number of favorites)
   tweets = db["twitter_posts"].find({"user_nickname" : nickname})
   retweets = []
   for t in tweets:
       retweets += t["retweets_list"]
   retweets = [r for r in retweets if r["text"]]
   top_10_retweets = sorted(retweets, key = lambda k : k["favorite_count"], reverse = True)[0:10]
   count = 1
   for t in top_10_retweets:
       #count-tweetMessage   
       tw_retweet_message_em = soup.find(id = str(count) + "-tweetMessage")
       tw_retweet_message_em.insert(0, t["text"])
       #count-tweetNumFavorites
       tw_retweet_num_favorites_em = soup.find(id = str(count) + "-tweetNumFavorites")
       tw_retweet_num_favorites_em.insert(0, "{:,}".format(t["favorite_count"]))
       count += 1
   
   #alertas, ofertas e oportunidades
   #alertas
   danger_fb = db["human_interactions"].find({"social_network" : "facebook", "user_nickname" : nickname, "reaction_type" : "danger"})
   for d in danger_fb:
       table_out = soup.find(id="FbThreatsTable")
       #comment's author
       #tr_tag_2 = soup.new_tag("tr")
       #td_likes_tag = soup.new_tag("td")
       #td_likes_tag.insert(0, "Author")
       #tr_tag_2.insert(0, td_likes_tag)
       #table_out.insert(0, tr_tag_2)

       #comment message
       tr_tag_1 = soup.new_tag("tr")
       td_message_tag = soup.new_tag("td")
       td_message_tag.insert(0, d["comment_text"])
       tr_tag_1.insert(0, td_message_tag)
       table_out.insert(0, tr_tag_1)
   
   #propostas de trabalho -> FbJobsProposal
   jobs_proposal_fb = db["human_interactions"].find({"social_network" : "facebook", "user_nickname" : nickname, "reaction_type" : "job's proposal" }) 
   for j in jobs_proposal_fb:
       #author_name = j["from"]["name"]
       table_out = soup.find(id="FbJobsProposal")
       tr_tag_1 = soup.new_tag("tr")
       td_message_tag = soup.new_tag("td")
       
       #h5_author_name = soup.new_tag("h5")
       #h5_author_name.insert(0, author_name)
       #td_message_tag.insert(0, h5_author_name)
       
       td_message_tag.insert(0, j["comment_text"])
       tr_tag_1.insert(0, td_message_tag)
       table_out.insert(0, tr_tag_1)

   file1 = open("html_pages/temporary/comments_" + nickname + ".html", "wb")
   file1.write(soup.prettify())
   file1.close()

def scrap_other_people_like_you(db, nickname):
    initialize(db, nickname)
    file1 = open("html_pages/total/other_profiles.html", "rb")
    html = file1.read()
    file1.close()
    soup = bs(html, "lxml")

    #current user's category
    user = db["users"].find({"nickname" : nickname, "is_admin" : False})
    user_category = ""
    for u in user:
       user_category = u["user_category"] 
    #Search for all user's nickname on the same category and append its nicknames to a dic of points
    # {"user1" : 0, "user2" : 0, "user3" : 0}
    #Criteria for ordenation -> number of fans, average reactions by post, average comments by post, average shares by post
    today_date = time.strftime("%Y-%m-%d")
    user_same_category_points = {}
    users_same_category = db["users"].find({"user_category" : user_category})
    for u in users_same_category:
        user_same_category_points[u["nickname"]] = 0
            
    for u in user_same_category_points.keys():
        #Number of followers, fans and talking about
        fb_followers_count, tw_followers_count, fb_talking_about_count = 0, 0, 0
        fb_average_comments_by_post, fb_average_shares_by_post, fb_average_reactions_by_post = 0, 0, 0
        tw_average_favorites_by_post, tw_average_retweets_by_post = 0, 0

        fb_user_about_row = db["about_collections"].find({"user_nickname" : u, "social_network" : "facebook"})
        tw_user_about_row = db["about_collections"].find({"user_nickname" : u, "social_network" : "twitter"})
        for f in fb_user_about_row:
            fb_followers_count = filter(lambda a : a.keys()[0] == today_date, f["fan_count_list"])[0][today_date]
            fb_talking_about_count = filter(lambda a : a.keys()[0] == today_date ,f["talking_about_count_list"])[0][today_date]

        for t in tw_user_about_row:
            tw_followers_count = filter(lambda a : a.keys()[0] == today_date, t["followers_count_list"])[0][today_date]
        
        #Average favorites by tweet, average retweets by tweet, average reactions by post, average comments by post, average shares by post
        fb_last_statistics = db["ready_statistics"].find({"user_nickname" : nickname, "social_network" : "facebook"})
        tw_last_statistics = db["ready_statistics"].find({"user_nickname" : nickname, "social_network" : "twitter"})
        for f in fb_last_statistics:
            fb_average_reactions_by_post = f["average_reactions_count"]
            fb_average_shares_by_post = f["average_shares_count"]
            fb_average_comments_by_post = f["average_comments_count"]
        for t in tw_last_statistics:
            tw_average_favorites_by_post = t["average_favorites_count"]
            tw_average_retweets_by_post = t["average_retweets_count"]
        
        followers_count = fb_followers_count + tw_followers_count + fb_talking_about_count
        posts_count = fb_average_comments_by_post + fb_average_shares_by_post + fb_average_reactions_by_post + tw_average_favorites_by_post + tw_average_retweets_by_post
        total_points = followers_count + posts_count
        user_same_category_points[u] = total_points

    #Order the dic by number of posts
    #[("nickname",0 ), ("nickname", 0)]
    sorted_user_same_category_points = sorted(user_same_category_points.items(), key = operator.itemgetter(1), reverse = True)    
    people_ranking_table = soup.find(id="PeopleRankingByCategory")
    position = 0
    for s in sorted_user_same_category_points:
         current_nickname = s[0]
         current_fullname = ""
         current_user_points = s[1]
         #position = sorted_user_same_category_points.index(s) + 1
         user_row = db["users"].find({"nickname" : current_nickname})
         for u in user_row:
            current_fullname = u["fullname"]
         if current_nickname == nickname:
             tr_tag_1 = soup.new_tag("tr")
             tr_tag_1.style = "background-color:lightgray;"
             td_position = soup.new_tag("td")
             td_fullname = soup.new_tag("td")
             td_points = soup.new_tag("td")
             td_position.insert(0, "#" + str(position + 1))
             td_fullname.insert(0, str(current_fullname))
             td_points.insert(0, "{:,}".format(current_user_points))
             tr_tag_1.insert(0, td_position)
             tr_tag_1.insert(1, td_fullname)
             tr_tag_1.insert(2, td_points)
             people_ranking_table.insert(position, tr_tag_1)
         else:
             tr_tag_1 = soup.new_tag("tr")
             td_position = soup.new_tag("td")
             td_fullname = soup.new_tag("td")
             td_points = soup.new_tag("td")
             td_position.insert(0, "#" + str(position + 1))
             td_fullname.insert(0, str(current_fullname))
             td_points.insert(0, "{:,}".format(current_user_points))
             tr_tag_1.insert(0, td_position)
             tr_tag_1.insert(1, td_fullname)
             tr_tag_1.insert(2, td_points)
             people_ranking_table.insert(position, tr_tag_1)   
         position += 1
    
    #Twitter trend topics


    file1 = open("html_pages/temporary/other_profiles_" + nickname + ".html", "wb")
    file1.write(soup.prettify())
    file1.close()

def scrap_summary(db, nickname):
   initialize(db, nickname)
   file1 = open("html_pages/total/summary.html", "rb")
   html = file1.read()
   file1.close()
   soup = bs(html, "lxml")  
   #FbMostFansCountry
   fb_most_fans_country_em = soup.find(id = "FbMostFansCountry")
   fb_most_fans_country_em.insert(0, fb_most_fans_country)
   #FbLessFansCountry
   fb_less_fans_country_em = soup.find(id="FbLessFansCountry")
   fb_less_fans_country_em.insert(0, fb_less_fans_country)
   #FbMostPostType
   fb_most_post_type_em = soup.find(id="FbMostPostType")
   fb_most_post_type_em.insert(0, fb_most_posted_type)
   #FbMostPopularPostType
   fb_most_popular_post_type_em = soup.find(id="FbMostPopularPostType")
   fb_most_popular_post_type_em.insert(0, fb_most_important_post_type_for_popularity )
   #FbOverallReaction
   fb_overall_reaction_em = soup.find(id = "FbOverallReaction")
   fb_overall_reaction_em.insert(0, fb_overall_reaction)
   #FbMostPostedDayOfWeek
   fb_most_posted_day_of_week_em = soup.find(id="FbMostPostedDayOfWeek")
   fb_most_posted_day_of_week_em.insert(0, fb_most_posted_day_of_week)
   #FbMostRepercutionDay
   fb_most_repercution_day_em = soup.find(id="FbMostRepercutionDay")
   fb_most_repercution_day_em.insert(0, fb_most_popular_day_of_week)
   #TwMostPostedDayOfWeek
   tw_most_posts_day_of_week_em = soup.find(id="TwMostPostedDayOfWeek")
   tw_most_posts_day_of_week_em.insert(0, tw_most_posts_day_of_week) 
   #TwMostRepercutionDay
   tw_most_repercution_day_of_week_em = soup.find(id="TwMostRepercutionDay")
   tw_most_repercution_day_of_week_em.insert(0, tw_most_popular_day_of_week)
   file1 = open("html_pages/temporary/summary_" + nickname + ".html", "wb")
   file1.write(soup.prettify())
   file1.close()
