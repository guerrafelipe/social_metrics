import time
import tweepy
from tweepy import OAuthHandler

def retrieve_profileinfo(consumer_key, consumer_secret, access_token, access_secret, user_id):
    auth = OAuthHandler(consumer_key,consumer_secret)
    api = tweepy.API(auth)
    auth.set_access_token(access_token, access_secret)
    api = tweepy.API(auth)
    user_object = api.lookup_users(user_ids=[ user_id ])
    profile_info = user_object[0]._json
    followers_count = profile_info["followers_count"]
    profile_info["social_network"] = "twitter"
    profile_info["followers_count_list"] = []
    profile_info["followers_count_list"].append({ time.strftime("%Y-%m-%d") : followers_count })
    return profile_info 

def get_followers_count(consumer_key, consumer_secret, access_token, access_secret, user_id):
    auth = OAuthHandler(consumer_key,consumer_secret)
    api = tweepy.API(auth)
    auth.set_access_token(access_token, access_secret)
    api = tweepy.API(auth)
    user_object = api.lookup_users(user_ids=[ user_id ])
    profile_info = user_object[0]._json
    return profile_info["followers_count"]