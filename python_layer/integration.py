# -*- encoding: utf-8 -*-
import facebook_layer.get_fb_posts_fb_page
import facebook_layer.get_fb_comments_from_fb
import settings, preprocess, main, analysis, reporter
import json
import sys, os, shutil, glob
import datetime
from clint.textui import colored
import tweepy
import twitter.main
import facebook_layer.main
import tweepy
from tweepy import OAuthHandler
from instagram.client import InstagramAPI
from pymongo import MongoClient
import urllib2
import time, datetime
from slacker import Slacker
import traceback

#Set the encoding
reload(sys)
sys.setdefaultencoding('UTF8')

"""
Erros de SSL significam que eh necessario criar uma nova app
"""

def do_integration(nickname, slack, host):
    
    client = MongoClient(host)
    db = client[settings.database_name]
    user_info = db.users.find({"nickname":nickname}) 
     
    if user_info:
        slack.chat.post_message("#" + "user_" + nickname.lower(), "Script initialized at " + str(datetime.date.today()))
        screen_name, fb_page_link, user_id, page_id = "", "", "", ""
        for user in user_info:
            print(colored.red("Coletando informacoes de:") + user["fullname"])
            screen_name = user["tw_scr_name"]
            fb_page_link = user["fb_page_link"]
            user_id = user["tw_user_id"]
            page_id = user["fb_page_id"]
            user_products = user["products"]
            if "cosmo" in user_products:
                is_premium = True
            else:
                is_premium = False
        if not page_id:
            page_name = fb_page_link.split("/")[-1]
            page_name = page_name.replace(":", "")
            print(fb_access_token)
            #https://graph.facebook.com/v2.6/tatawerneck/?fields=id&access_token=1237688692929808|498e47472b971497924225f0168cbf43
            graph_url = "https://graph.facebook.com/v2.6/%s/?fields=id&access_token=%s" % (page_name, fb_access_token)
         
            req = urllib2.Request(graph_url)
            success = False
            while success is False:
                try: 
                    response = urllib2.urlopen(req)
                    if response.getcode() == 200:
                        success = True
                except Exception, e:
                    print e
                    time.sleep(5)    
                    print "Error for URL %s %s" % (graph_url, datetime.datetime.now())
                    print "Retrying."
            # retrieve data
            data = json.load(response)
            page_id = data["id"]
            print("Page ID:" + colored.blue(page_id))
            db.users.update({"nickname" : nickname}, {"$set" : {"fb_page_id" : page_id}})
        else:
            print(colored.green("Page ID ja configurado anteriormente"))

        #Get twitter user_id
        if not user_id:
            settings.tw_credentials()
            auth = OAuthHandler(settings.consumer_key, settings.consumer_secret)
            auth.set_access_token(settings.access_token, settings.access_secret)
            api = tweepy.API(auth, wait_on_rate_limit = True, wait_on_rate_limit_notify = True)     	
            user_object = api.get_user(screen_name)
            user_object = user_object._json
            user_id = user_object["id"]
            print("User ID:" + colored.blue(user_id))
            db.users.update({"nickname" : nickname}, {"$set" : {"tw_user_id" : user_id}})
        else:
            print(colored.green("User ID ja configurado anteriormente"))

        ready_statistics_tw = db.ready_statistics.find({"user_nickname" : nickname, "created_at" : time.strftime("%Y-%m-%d"), "social_network" : "twitter"}).count()
        ready_statistics_fb = db.ready_statistics.find({"user_nickname" : nickname, "created_at" : time.strftime("%Y-%m-%d"), "social_network" : "facebook"}).count()
        listed_posts_fb = db.job_stacks.find({"social_network" : "facebook", "user_nickname" : nickname})
        listed_tweets_tw = db.job_stacks.find({"social_network" : "twitter", "user_nickname" : nickname})

        """
        PROCCESS 0: Feed train model
        """
        if ready_statistics_tw == 0 and ready_statistics_fb == 0:
            analysis.feed_trainmodel()
            slack.chat.post_message("#user_" + nickname, "Training model fed")
        else:
            print(colored.green("Modelo de treino ja alimentado anteriormente"))
    
        """
        PROCCESS 1: Get facebook posts
        """ 
        if listed_posts_fb.count() == 0:
            """
            Download and index all posts
            """
            list_posts = facebook_layer.get_fb_posts_fb_page.listFacebookPagePosts(page_id, is_premium, fb_access_token, nickname) 
             
            db["job_stacks"].insert(list_posts)
            list_posts_count = len(list_posts["posts"])
            print(colored.green("Pilha de posts criada"))
            slack.chat.post_message("#user_" + nickname, "Facebook posts were indexed successfully!")
                
            print("Numero de posts do usuario:" + colored.blue(list_posts_count))
            #estimar tempo para baixar todos os posts
            slack.chat.post_message("#user_" + nickname, "ANSWER THE QUESTION")
            answer_number = raw_input("Digite o numero maximo de posts que voce deseja baixar:")
            answer_number = int(answer_number)
            
            if answer_number < list_posts_count:
                list_posts = list_posts["posts"] 
                list_posts = [i["id"] for i in list_posts]
                main.get_fbposts(db, nickname, page_id, fb_access_token, "partial", list_posts[0:answer_number], is_premium, slack)              
            elif answer_number == list_posts_count:
                main.get_fbposts(db, nickname, page_id, fb_access_token, "total", is_premium, slack)
            else:
                print(colored.red("Quantidade errada!"))
        else:
            """
            Download and index only new posts
            """
            for l in listed_posts_fb:
                #listed_posts_fb -> old version (SET A)
                listed_posts_fb = l["posts"]

            #listed_posts_fb = filter(lambda p: p["downloaded"] == True, listed_posts_fb) -> SET A 
            old_listed_posts_fb_indexes = set([i["id"] for i in listed_posts_fb])
            
            #new_listed_posts_fb -> SET B
            new_listed_posts_fb = facebook_layer.get_fb_posts_fb_page.listFacebookPagePosts(page_id, is_premium, fb_access_token, nickname) 
            new_listed_posts_fb_indexes = set([i["id"] for i in new_listed_posts_fb["posts"]])
            
            #Baixar apenas esses indexes
            real_new_listed_posts_fb_indexes = new_listed_posts_fb_indexes - old_listed_posts_fb_indexes            
            
            for x in real_new_listed_posts_fb_indexes:
                #{id:integer, downloaded: boolean, comments_analysed:boolean}
                dic = {}
                dic["id"] = x
                dic["downloaded"] = False
                dic["comments_analysed"] = False
                dic["created_at"] = time.strftime("%Y-%m-%d")
                db.job_stacks.update({"social_network" : "facebook", "user_nickname" : nickname}, {"$push" : {"posts" : dic}})
            print(colored.green("Indexando apenas os novos posts"))
           
            """
            Get posts indexed, but not downloaded ({"downloaded" : False})
            """
            #Not downloaded yet
            user_stack = db.job_stacks.find({ "social_network" : "facebook", "user_nickname" : nickname})
            for u in user_stack:
                user_stack = u["posts"]     
            user_stack = filter(lambda b: b["downloaded"] == False, user_stack)
            user_stack = set([i["id"] for i in user_stack])
            print(colored.green("Pegando apenas os posts nao baixados ainda"))            
            
            list_to_get = user_stack | real_new_listed_posts_fb_indexes
            list_to_get = filter(lambda a: a, list_to_get) #remove empty entries
            
            print("Numero de posts pra baixar:" + colored.blue(len(list_to_get)))
            slack.chat.post_message("#user_" + nickname, "ANSWER THE QUESTION")
            answer_number = raw_input("Digite o numero maximo de posts que voce deseja baixar:")
            answer_number = int(answer_number)
            if answer_number < len(list_to_get): 
                main.get_fbposts(db, nickname, page_id, fb_access_token, "partial", list_to_get[0:answer_number], is_premium, slack)    
            elif answer_number == len(list_to_get):
                main.get_fbposts(db, nickname, page_id, fb_access_token, "partial", list_to_get, is_premium, slack)
            else:
                print(colored.red("Quantidade errada!"))

        slack.chat.post_message("#user_" + nickname, "Facebook posts downloaded successfully")
    
        """
        PROCCESS 2: Analyse facebook posts
        """
        analyse_fb_posts = raw_input(colored.blue("Deseja analisar os posts do facebook agora?(y/n):"))
        if analyse_fb_posts == "y":
            #today_posts = db["job_stacks"].find({"social_network" : "facebook", "user_nickname" : nickname, "posts.downloaded" : True, "posts.comments_analysed" : False})       
            
            if ready_statistics_fb == 0:
                if db["ready_statistics"].find({"social_network" : "facebook", "user_nickname" : nickname}).count() == 0:
                    #First analysis -> Analyse all downloaded posts
                    analysis.analyse_facebook(db, nickname, page_id, fb_access_token, slack, "total")
                else:
                    #Other analysis -> Analyse only last 7 days posts
                    analysis.analyse_facebook(db, nickname, page_id, fb_access_token, slack, "partial")
            else:
                print(colored.green("A analise dos posts do Facebook ja foi feita hoje"))
        
        """
        PROCCESS 3: Get twitter posts
        """   

        if listed_tweets_tw.count() == 0:
            db["job_stacks"].insert({"social_network" : "twitter", "user_nickname" : nickname, "posts" : []})
        main.get_twposts(db, is_premium, nickname, screen_name, tw_consumer_key, tw_consumer_secret, tw_access_token, tw_access_secret, user_id, slack)
        
        slack.chat.post_message("#user_" + nickname, "Tweets downloaded successfully")
    
        """
        PROCCESS 4: Analyse twitter posts
        """
        analyse_tw_posts = raw_input(colored.blue("Deseja analisar os tweets agora?(y/n):"))
        if analyse_tw_posts == "y":
            if ready_statistics_tw == 0:
                if db["ready_statistics"].find({"social_network" : "twitter", "user_nickname" : nickname}).count() == 0:
                    analysis.analyse_twitter(db, nickname, slack, "total")
                else:
                    analysis.analyse_twitter(db, nickname, slack, "partial")
            else:
                print(colored.green("Analise de tweets ja feita anteriormente"))

        """
        PROCCESS5: Generate reports if analyse_posts == true
        """
        if analyse_fb_posts == "y" and analyse_tw_posts == "y":
            if ready_statistics_tw == 0 and ready_statistics_fb == 0:
                #total
                reporter.generate_pdf(db, nickname, "total")
            else:
                #partial
                reporter.generate_pdf(db, nickname, "partial")

        """
        PROCCESS6: Clear temporary files
        """
        if analyse_fb_posts == "y" and analyse_tw_posts == "y":
            #remove plots
            os.chdir("html_pages")
            shutil.rmtree("plots")
            os.mkdir("plots")
            print(colored.green("Arquivos da posta /html_pages/plots removidos"))
            
            #remove temporary (.html)
            os.chdir("temporary")
            html_files = glob.glob("*.html")
            for f in html_files:
                os.remove(f)
            os.chdir("..")
            print(colored.green("Arquivos .html da pasta /html_pages/temporary removidos"))
    else:
	    print(colored.red("Nenhum usuario encontrado!! :("))

if __name__ == "__main__":
    env = sys.argv[1]
    #facebook credentials
    settings.fb_credentials()
    fb_access_token = settings.access_token
    #slack credentials
    settings.slack_credentials()
    slack_api_token = settings.api_token
    #twitter credentials
    settings.tw_credentials()
    tw_consumer_key = settings.consumer_key
    tw_consumer_secret = settings.consumer_secret
    tw_access_token = settings.access_token
    tw_access_secret = settings.access_secret
    settings.environment(env)
    print(colored.red("Modo de execucao:") + str(env))
    print(colored.red("ATENCAO: ") + "Nao esqueca de criar o canal no Slack (#user_nickname)")
    nickname = raw_input(colored.blue("Digite um 'nickname':"))    
        
    slack = Slacker(slack_api_token)
    slack.chat.post_message("#python_layer", "Script initialized at " + str(datetime.date.today()) + " for " + str(nickname))
    
    try:
        do_integration(nickname, slack, settings.host)
    except Exception, e:
        traceback.print_exc()
        slack.chat.post_message("#python_layer", "Script ERROR at " + str(datetime.date.today()) + " for " + str(nickname) + ". Error message: " + str(e))
        slack.chat.post_message("#user_" + nickname, "Script ERROR at" + str(datetime.date.today()) + " for " + str(nickname) + ". Error message:" + str(e))