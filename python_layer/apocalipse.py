from pymongo import MongoClient
import settings
from clint.textui import colored

def do_apocalipse():
	settings.environment()
	client = MongoClient()
	db = client[settings.database_name]
	db["about_collections"].remove({})
	db["facebook_posts"].remove({})
	db["human_interactions"].remove({})
	db["job_stacks"].remove({})
	db["posts_timer"].remove({})
	db["ready_statistics"].remove({})
	db["twitter_posts"].remove({})

	print(colored.green("Dados removidos!"))

if __name__ == "__main__":
	do_apocalipse()