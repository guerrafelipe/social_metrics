import facebook
import urllib2
import json
import time
import settings

#Other pages liked by this page listed
def get_fb_page_likes_lists(fb_page_id, access_token):
    graph = facebook.GraphAPI(access_token = access_token)
    args = {'fields': 'likes'}
    page = graph.get_object(fb_page_id, **args)
    print(page)
    return page.get('likes', 0)["data"]


def get_page_fans_country(fb_page_id, access_token):
    graph_url = "https://graph.facebook.com/v2.7/%s/insights/page_fans_country?access_token=%s" %(fb_page_id, access_token)
    req = urllib2.Request(graph_url)
    success = False
    while success is False:
        try: 
            response = urllib2.urlopen(req)
            if response.getcode() == 200:
                success = True
        except Exception, e:
            print e
            time.sleep(5)    
            print "Error for URL %s: %s" % (graph_url, datetime.datetime.now())
            print "Retrying."
        
    # retrieve data
    json_data = json.load(response)
    countries_dic = json_data["data"]
    if len(countries_dic) > 1:
        countries_dic = json_data["data"][-1]["values"]
    else:
        countries_dic = json_data["data"][0]["values"]
    
    #{"end_time" : "", "value" : {"BR" : 70}}
    last_countries_dic = countries_dic[-1]    
    return last_countries_dic

#Number of people who like this page
def get_fb_page_like_count(fb_page_id, access_token):
    graph = facebook.GraphAPI(access_token = access_token)
    args = {'fields': 'fan_count'}
    page = graph.get_object(fb_page_id, **args)
    return page.get('fan_count', 0)

#Number of people who like this page
def get_fb_page_posts_count(fb_page_id, access_token):
    graph = facebook.GraphAPI(access_token = access_token)
    args = {'fields': 'posts_count'}
    page = graph.get_object(fb_page_id, **args)
    return page.get('fan_count', 0)

#Number of people who are talking about this page
def get_fb_page_talking_about_count(fb_page_id, access_token):
    graph = facebook.GraphAPI(access_token = access_token)
    args = {'fields': 'talking_about_count'}
    page = graph.get_object(fb_page_id, **args)
    return page.get('talking_about_count', 0)

#This Page's profile picture. No access token is required to access this edge
def get_fb_page_picture(fb_page_id, access_token):
    graph = facebook.GraphAPI(access_token = access_token)
    args = {'fields': 'picture'}
    page = graph.get_object(fb_page_id, **args)
    return page.get('picture', 0)["data"]

def get_fb_page_description(fb_page_id, access_token):
    graph = facebook.GraphAPI(access_token = access_token)
    args = {'fields': 'description'}
    page = graph.get_object(fb_page_id, **args)
    return page.get('description', 0)

def retrieve_profileinfo(fb_page_id, access_token):
    profile_info = {}  
    #likes_list = get_fb_page_likes_lists(fb_page_id, access_token)
    num_likes = get_fb_page_like_count(fb_page_id, access_token)
    num_talking_about = get_fb_page_talking_about_count(fb_page_id, access_token)
    description = get_fb_page_description(fb_page_id, access_token)
    picture = get_fb_page_picture(fb_page_id, access_token)
    profile_info["social_network"] = "facebook"
    
    #Fan count list
    profile_info["fan_count_list"] = []
    profile_info["fan_count_list"].append({ time.strftime("%Y-%m-%d") : num_likes})
    
    #Talking about list
    profile_info["talking_about_count_list"] = []
    profile_info["talking_about_count_list"].append({time.strftime("%Y-%m-%d") : num_talking_about})

    profile_info["picture"] = picture
    profile_info["description"] = description
    return profile_info

if __name__ == "__main__":
    settings.fb_credentials()
    get_page_fans_country(settings.page_id, settings.access_token)