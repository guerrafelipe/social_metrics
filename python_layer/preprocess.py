import string
import re
from nltk.tokenize import word_tokenize

#Identify mentions and hashtags
emoticons_str = r"""
    (?:
        [:=;] # Eyes
        [oO\-]? # Nose (optional)
        [D\)\]\(\]/\\OpP] # Mouth
    )""" 
regex_str = [
    emoticons_str,
    r'<[^>]+>', # HTML tags
    r'(?:@[\w_]+)', # @-mentions
    r"(?:\#+[\w_]+[\w\'_\-]*[\w_]+)", # hash-tags
    r'http[s]?://(?:[a-z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-f][0-9a-f]))+', # URLs
 
    r'(?:(?:\d+,?)+(?:\.?\d+)?)', # numbers
    r"(?:[a-z][a-z'\-_]+[a-z])", # words with - and '
    r'(?:[\w_]+)', # other words
    r'(?:\S)' # anything else
]

tokens_re = re.compile(r'('+'|'.join(regex_str)+')', re.VERBOSE | re.IGNORECASE)
emoticon_re = re.compile(r'^'+emoticons_str+'$', re.VERBOSE | re.IGNORECASE)

def smart_stripper(text):
    return " ".join(word_tokenize(text))

def tokenize(s):
    return tokens_re.findall(s)

def process(s, lowercase = False):
    tokens = tokenize(s)
    if lowercase:
        tokens = [token if emoticon_re.search(token) else token.lower() for token in tokens]
    return tokens

def get_hashtags(text_splited):
    hashtags_list = []
    for t in text_splited:
        if t[0] == "#":
            hashtags_list.append(t)
        else:
            continue
    return hashtags_list

def get_mentions(text_splited):
    mentions_list = []
    for t in text_splited:  
        if t[0] == "@":
            mentions_list.append(t)
        else:
            continue
    return mentions_list

def convert2unicode(str1):
    try:
        str1 = unicode(str1, errors = 'replace')
        return str1
    except:
        return str1

def remove_punctuaction(pre_text):
    #pos_text = pre_texto.translate(string.maketrans(string.punctuation, ""), string.punctuation)
    after_text = "".join(l for l in pre_text if l not in string.punctuation)
    return after_text

