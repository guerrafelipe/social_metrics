from __future__ import division
import sys, os, glob
import time
import nltk
import datetime
from datetime import date
from datetime import timedelta
from pymongo import MongoClient
import settings
from clint.textui import colored
import preprocess, settings
import nltk
from nltk.tokenize import word_tokenize
from nltk.tokenize import wordpunct_tokenize
from nltk.corpus import stopwords
from nltk.collocations import *
from nltk.collocations import BigramCollocationFinder
from nltk.metrics import BigramAssocMeasures
from nltk.classify.scikitlearn import SklearnClassifier
from nltk.classify import ClassifierI
import sklearn
import sklearn.datasets
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn import metrics
import numpy as np
from sklearn.externals import joblib
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.linear_model import SGDClassifier
from sklearn.pipeline import Pipeline
import gspread
from oauth2client.service_account import ServiceAccountCredentials
import facebook_layer.main

reload(sys)  # Reload does the trick!
sys.setdefaultencoding('UTF8')

"""
    if added_count > 0:
        #50 most common hashtags
        fdist1 = nltk.FreqDist(globalHashtags_list)
        print("Hashtags mais comuns:" + colored.yellow(fdist1.most_common(50)))
        print("Hashtag mais comum:" + colored.yellow(fdist1.max()))

        #50 most common mentions
        at_scrname = "@" + settings.scr_name
        globalMentions_list.remove(at_scrname)
        fdist2 = nltk.FreqDist(globalMentions_list)
        print("Mencoes mais comuns:" + colored.green(fdist2.most_common(50)))
        print("Mencao mais comum:" + colored.green(fdist2.max()))
    
        #50 most common words
        raw_messages = remove_punctuaction(raw_messages)
        raw_messages = raw_messages.lower()
        raw_messages = word_tokenize(raw_messages)
        stopwords = nltk.corpus.stopwords.words(settings.language)
        for sw in stopwords:
            if sw in raw_messages:
                raw_messages.remove(sw)
        fdist3 = nltk.FreqDist(raw_messages)
        print("Palavras mais comuns:" + colored.red(fdist3.most_common(50)))
        print("Palavra mais comum:" + colored.red(fdist3.max()))
"""

def return_topwords(text):
   text = preprocess.remove_punctuaction(text)
   text = text.lower()
   text = nltk.word_tokenize(text)
   stopwords = nltk.corpus.stopwords.words(settings.language)
   stopwords_english = nltk.corpus.stopwords.words("english")   
   for sw in stopwords:
       if sw in text:
           text = filter(lambda a: a != sw, text)           
   for sw in stopwords_english:
       if sw in text:
           text = filter(lambda a: a != sw, text)           
   fdist3 = nltk.FreqDist(text)
   return fdist3
   
#Feed train_model with google docs spreadsheet
def feed_trainmodel():
    scope = ['https://spreadsheets.google.com/feeds']
    credentials = ServiceAccountCredentials.from_json_keyfile_name('client_secret.json', scope)    
    gc = gspread.authorize(credentials)    
    wks = gc.open("Reactions").get_worksheet(0)        
    comments = wks.col_values(1)   
    comments_reaction = wks.col_values(2)    
    positive_text, negative_text, neutral_text = "", "", ""
    for x, y in zip(comments, comments_reaction):
        if x and y:
            reaction = y.lower()
            comment = x.lower()
            if reaction == "positiva":
                positive_text += comment
            elif reaction == "negativa":
               negative_text += comment
            elif reaction == "neutra":
               neutral_text += comment
            else:
                continue
        else:
            continue
    #positive text    
    positive_text = positive_text.lower()    
    positive_text = preprocess.remove_punctuaction(positive_text)
    positive_text = word_tokenize(positive_text)
    
    #negative text
    negative_text = negative_text.lower()   
    negative_text = preprocess.remove_punctuaction(negative_text)
    negative_text = word_tokenize(negative_text)
    
    #neutral text
    neutral_text = neutral_text.lower()    
    neutral_text = preprocess.remove_punctuaction(neutral_text)
    neutral_text = word_tokenize(neutral_text)
    
    stopwords = nltk.corpus.stopwords.words("portuguese")
    for sw in stopwords:
        if sw in positive_text:
            #filter -> Construct a list from those elements of iterable for which function returns true
            positive_text = filter(lambda a: a != sw, positive_text)
        if sw in negative_text:
            negative_text = filter(lambda a: a != sw, negative_text)
        if sw in neutral_text:
            neutral_text = filter(lambda a: a != sw, neutral_text)
    positive_text = " ".join(positive_text)
    negative_text = " ".join(negative_text)
    neutral_text = " ".join(neutral_text)
    
    try:
        if os.path.isdir("pt_train_model"):
            os.chdir("pt_train_model")
        else:
            os.mkdir("pt_train_model")
            os.chdir("pt_train_model")
    
        #adds positive reactions
        if os.path.isdir("positive"):
            os.chdir("positive")
        else:
            os.mkdir("positive")
            os.chdir("positive")
        file1 = open(time.strftime("%d:%m:%Y" + ".txt"), "wb") 
        file1.write(positive_text)
        file1.close()
        os.chdir("..")

        #adds negative reactions
        if os.path.isdir("negative"):
            os.chdir("negative")
        else:
            os.mkdir("negative")
            os.chdir("negative")
        file1 = open(time.strftime("%d:%m:%Y" + ".txt"), "wb") 
        file1.write(negative_text)
        file1.close()
        os.chdir("..")

        #adds neutral reactions
        if os.path.isdir("neutral"):
            os.chdir("neutral")
        else:
            os.mkdir("neutral")
            os.chdir("neutral")
        file1 = open(time.strftime("%d:%m:%Y" + ".txt"), "wb") 
        file1.write(neutral_text)
        file1.close()
        os.chdir("..")
        os.chdir("..")
        print(colored.green("Arquivos criados com sucesso"))
    except:
        print(colored.red("Algo deu errado"))
    
def get_equal_posts(db):
    facebook_posts = db["facebook_posts"].find()
    twitter_posts = db["twitter_posts"].find() 
    for document in facebook_posts:
        facebook_message = document["status_message"].lower()
        facebook_message = preprocess.smart_stripper(facebook_message)
        for document in twitter_posts:
            twitter_message = document["text"].lower()
            twitter_message = preprocess.smart_stripper(twitter_message)
            if facebook_message == twitter_message:
                print("____________________________________________")                
                print(colored.green("Posts iguais no texto:"))
                print(colored.blue("Facebook:") + facebook_message)
                print(colored.red("Twitter:") + twitter_message)
                print("____________________________________________")
            else:
                continue

def analyse_reaction(test_data, path = "pt_train_model"):    
    test_data_list = []
    test_data = test_data.lower()
    test_data = preprocess.remove_punctuaction(test_data)
    test_data_list.append(test_data)
    
    train_data = sklearn.datasets.load_files(path, load_content = True, shuffle = True, encoding="utf-8")
    count_vect = CountVectorizer()
    X_train_counts = count_vect.fit_transform(train_data.data)
    tfidf_transformer = TfidfTransformer()
    X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)    
    clf = MultinomialNB().fit(X_train_tfidf, train_data.target)
    #TEST DATA
    X_new_counts = count_vect.transform(test_data_list)
    X_new_tfidf = tfidf_transformer.transform(X_new_counts)   
    predicted = clf.predict(X_new_tfidf)

    news_classified = {}
    for doc, category in zip(test_data_list, predicted):
          news_classified[doc] = train_data.target_names[category]    
    return news_classified

def analyse_twitter(db, nickname, slack, mode):
   raw_tweets, raw_retweets = "", ""
   print("________________________________________________")
   print(colored.green("Twitter"))   
   human_interactions = db["human_interactions"]
   ready_statistics = db["ready_statistics"]
   
   dates_to_analyse = []
   twitter_count = 0
   if mode == "partial":
       #Analyse only the tweets of the last 7 days
       i = datetime.datetime.now()
       today = date(i.year, i.month , i.day)
       for i in range(0, 7):
           dates_to_analyse.append(str(today - timedelta(days = i)))
       #Number of tweets in period of this time
       for d in dates_to_analyse:
           twitter_count += db["twitter_posts"].find({"created_at" : d, "user_nickname" : nickname}).count() 
   else:
       twitter_count = db["twitter_posts"].find({"user_nickname" : nickname}).count()

   print(colored.blue("->Numero de posts:") + str(twitter_count))       
   slack.chat.post_message("#user_" + nickname, "ANSWER THE QUESTION") 
   answer = raw_input("Digite o numero maximo de retweets que deseja baixar:")
   answer = int(answer)
   
   favorites_count, retweets_count  = 0, 0
   post_ids = []
   job_stack = db["job_stacks"].find({"social_network" : "twitter", "user_nickname" : nickname})   
   for j in job_stack:
        posts = j["posts"]
        if mode == "partial":
            for d in dates_to_analyse:
                post_ids += [i["id"] for i in posts if i["comments_analysed"] == False and i["downloaded"] == True and i["created_at"] == d]            
        else:
            post_ids = [i["id"] for i in posts if i["comments_analysed"] == False and i["downloaded"] == True]            
   twitter_posts = []
   for p in post_ids:    
       post_row = db["twitter_posts"].find({ "id" : p, "user_nickname" : nickname})
       for g in post_row:
           twitter_posts.append(g)

   #Get twitter screen name
   about_user = db["about_collections"].find({"user_nickname" : nickname, "social_network" : "twitter"})
   tw_screen_name = ""
   for a in about_user:
       tw_screen_name = a["screen_name"]
   text_to_remove = "@" + tw_screen_name + ":"
   raw_positivereacs, raw_negativereacs, raw_neutralreacs = "", "", ""  
   positivereacs_number, negativereacs_number, neutralreacs_number = 0, 0, 0
   for t in twitter_posts:
        status_id = t["id"]
        tweet_text = t["text"]
        favorites_count += t["favorite_count"]
        retweets_count += t["retweet_count"]
        raw_tweets += tweet_text
        count = 0
        for x in t["retweets_list"]:
            if count <= answer:
                retweet_text = x["text"]            
                comparation_retweet_text = retweet_text.replace("RT", "")
                comparation_retweet_text = retweet_text.replace(text_to_remove, "")

                if tweet_text.lower() == comparation_retweet_text.lower():
                    continue
                else:
                    raw_retweets += retweet_text
                    print("->Tweet Analisado:" + colored.green(retweet_text))
                    retweet_reaction = analyse_reaction(retweet_text)
                    if retweet_reaction.values()[0] == "positive":
                        raw_positivereacs += retweet_text
                        human_interactions.insert({"admin_nickname" : "", "comment_text" : retweet_text, "social_network" : "twitter", "reaction_type" : "positive", "user_nickname" : nickname, "already_validated" : False, "is_right" : False})     
                        positivereacs_number += 1
                        print("  Reacao:" + colored.green("Positiva"))
                    elif retweet_reaction.values()[0] == "negative":
                        raw_negativereacs += retweet_text
                        human_interactions.insert({"admin_nickname" : "", "comment_text" : retweet_text, "social_network" : "twitter", "reaction_type" : "negative", "user_nickname" : nickname, "already_validated" : False, "is_right" : False})           
                        negativereacs_number += 1
                        print("  Reacao:" + colored.red("Negativa"))
                    else:
                        raw_neutralreacs += retweet_text
                        human_interactions.insert({"admin_nickname" : "", "comment_text" : retweet_text, "social_network" : "twitter", "reaction_type" : "neutral", "user_nickname" : nickname, "already_validated" : False, "is_right" : False})
                        neutralreacs_number += 1
                        print("  Reacao:" + colored.yellow("Neutra"))
                    count += 1
            else:
              continue
        db["job_stacks"].update({"user_nickname" : nickname, "social_network" : "twitter", "posts.id" : status_id}, {"$set" : { "posts.$.comments_analysed" : True }})
   
   if retweets_count:
       average_positivereacs = round((positivereacs_number / retweets_count) * 100)
       average_negativereacs = round((negativereacs_number / retweets_count) * 100)
       average_neutralreacs = round((neutralreacs_number / retweets_count) * 100)    
       average_retweets = round((retweets_count / twitter_count) * 100)
       print(colored.blue("  Numero medio de retweets:") + str(average_retweets)) 
       print(colored.blue("  Numero medio de reacoes positivas:") + positivereacs_number + "(" + str(average_positivereacs) + "%)")
       print(colored.blue("  Numero medio de reacoes negativas:") + negativereacs_number + "(" + str(average_negativereacs) + "%)")
       print(colored.blue("  Numero medio de reacoes neutras:") + neutralreacs_number + "(" + str(average_neutralreacs) + "%)")      
   
   if favorites_count:  
       average_favorites = round((favorites_count / twitter_count) * 100)
       print(colored.blue("  Numero medio de favoritos:") + str(average_favorites)) 

   print("OBS: Analisamos a reacao dos retweets que contem um comentario a mais, comparado ao tweet de origem")   
   
   if raw_tweets:
       #tweets   
       fdist_tweets = return_topwords(raw_tweets) if return_topwords(raw_tweets) else []
       print("Palavras mais comuns nos tweets:" + colored.red(fdist_tweets.most_common(50)))
       print("Palavra mais comum nos tweets:" + colored.red(fdist_tweets.max())) 
   
   if raw_retweets:  
       #retweets
       fdist_retweets = return_topwords(raw_retweets) if return_topwords(raw_retweets) else []
       print("Palavras mais comuns nos retweets:" + colored.red(fdist_retweets.most_common(50)))
       print("Palavra mais comum nos retweets:" + colored.red(fdist_retweets.max())) 
   
   if raw_positivereacs:
       #palavras mais comuns nos retweets positivos
       fdist = return_topwords(raw_positivereacs) if return_topwords(raw_positivereacs) else []
       print("Palavras mais comuns nos retweets positivos:" + colored.red(fdist.most_common(50)))
       print("Palavra mais comum nos retweets positivos:" + colored.red(fdist.max())) 
   
   if raw_negativereacs:
       #palavras mais comuns nos retweets negativos 
       fdist = return_topwords(raw_negativereacs) if return_topwords(raw_negativereacs) else []
       print("Palavras mais comuns nos retweets negativos:" + colored.red(fdist.most_common(50)))
       print("Palavra mais comum nos retweets negativos:" + colored.red(fdist.max())) 
   
   if raw_retweets:
       if mode == "partial":    
           ready_statistics.insert({ "average_favorites_count" : average_favorites, "average_retweets_count" : average_retweets, "retweets_count" : retweets_count, "first_statistic" : False, "comments_average_neutralreacs" : average_neutralreacs, "comments_average_negativereacs" : average_negativereacs, "comments_average_positivereacs" : average_positivereacs , "words_fdist_tweets" : fdist_tweets, "words_fdist_retweets" : fdist_retweets, "created_at" : time.strftime("%Y-%m-%d"), "social_network" : "twitter", "user_nickname" : nickname, "posts_count" : twitter_count }) 
       else:
           ready_statistics.insert({"average_favorites_count" : average_favorites, "average_retweets_count" : average_retweets, "retweets_count" : retweets_count, "first_statistic" : True, "comments_average_neutralreacs" : average_neutralreacs, "comments_average_negativereacs" : average_negativereacs, "comments_average_positivereacs" : average_positivereacs , "words_fdist_tweets" : fdist_tweets, "words_fdist_retweets" : fdist_retweets, "created_at" : time.strftime("%Y-%m-%d"), "social_network" : "twitter", "user_nickname" : nickname, "posts_count" : twitter_count }) 
   else:
       if mode == "partial":
           ready_statistics.insert({ "average_favorites_count" : average_favorites, "average_retweets_count" : average_retweets, "retweets_count" : retweets_count, "first_statistic" : False, "comments_average_neutralreacs" : 0, "comments_average_negativereacs" : 0, "comments_average_positivereacs" : 0 , "words_fdist_tweets" : fdist_tweets, "words_fdist_retweets" : [], "created_at" : time.strftime("%Y-%m-%d"), "social_network" : "twitter", "user_nickname" : nickname, "posts_count" : twitter_count }) 
       else:
           ready_statistics.insert({ "average_favorites_count" : average_favorites, "average_retweets_count" : average_retweets, "retweets_count" : retweets_count, "first_statistic" : True, "comments_average_neutralreacs" : 0, "comments_average_negativereacs" : 0, "comments_average_positivereacs" : 0 , "words_fdist_tweets" : fdist_tweets, "words_fdist_retweets" : [], "created_at" : time.strftime("%Y-%m-%d"), "social_network" : "twitter", "user_nickname" : nickname, "posts_count" : twitter_count })
   print("________________________________________________")

def analyse_facebook(db, nickname, fb_page_id, access_token, slack, mode):
    print("________________________________________________")
    print(colored.green("Facebook"))

    human_interactions = db["human_interactions"]
    ready_statistics = db["ready_statistics"]
    countries_distribution_dic = facebook_layer.main.get_page_fans_country(fb_page_id, access_token)
    
    dates_to_analyse = []
    facebook_count = 0
    photo_count, video_count, link_count, status_count = 0, 0, 0, 0
    if mode == "partial":
        #Analyse only the posts of the last 7 days    
        i = datetime.datetime.now()
        today = date(i.year, i.month , i.day)
        for i in range(0, 7):
            dates_to_analyse.append(str(today - timedelta(days = i)))
        #Number of posts in period of this time
        for d in dates_to_analyse:
            facebook_count += db["facebook_posts"].find({"status_created_date" : d, "user_nickname" : nickname}).count()
            photo_count += db["facebook_posts"].find({"status_created_date" : d, "status_type" : "photo", "user_nickname" : nickname}).count()    
            video_count += db["facebook_posts"].find({"status_created_date" : d, "status_type" : "video", "user_nickname" : nickname}).count()
            link_count += db["facebook_posts"].find({"status_created_date" : d, "status_type" : "link", "user_nickname" : nickname}).count()
            status_count += db["facebook_posts"].find({"status_created_date" : d, "status_type" : "status", "user_nickname" : nickname}).count()
    else:
        facebook_count = db["facebook_posts"].find({"user_nickname" : nickname}).count()
        photo_count = db["facebook_posts"].find({"status_type" : "photo", "user_nickname" : nickname}).count() 
        video_count = db["facebook_posts"].find({"status_type" : "video", "user_nickname" : nickname}).count()
        link_count = db["facebook_posts"].find({"status_type" : "link", "user_nickname" : nickname}).count()
        status_count = db["facebook_posts"].find({"status_type" : "status", "user_nickname" : nickname}).count()

    print(colored.blue("->Numero de posts do Facebook:") + str(facebook_count))    
    
    p_photos = round((photo_count / facebook_count) * 100)
    print(colored.blue("  Fotos:") + str(photo_count)  + " (" +  str(p_photos) + "%)" )    
    
    p_videos = round((video_count / facebook_count) * 100)
    print(colored.blue("  Videos:") + str(video_count)  + " (" +  str(p_videos) + "%)" )    
    
    p_links = round((link_count / facebook_count) * 100)
    print(colored.blue("  Links:") + str(link_count)  + " (" +  str(p_links) + "%)")    
    
    p_status = round((status_count / facebook_count) * 100)
    print(colored.blue("  Status:") + str(status_count) + " (" +  str(p_status) + "%)")   
    
    slack.chat.post_message("#user_" + nickname, "ANSWER THE QUESTION") 
    answer = raw_input("Digite o numero maximo de comentarios por post que voce deseja analisar:")
    answer = int(answer)
    
    job_stack = db["job_stacks"].find({"social_network" : "facebook", "user_nickname" : nickname})

    posts_ids = []
    comments_number, reactions_number = 0, 0  
    for j in job_stack:
      posts = j["posts"]
      if mode == "partial":
          for d in dates_to_analyse:  
            posts_ids += [i["id"] for i in posts if i["comments_analysed"] == False and i["downloaded"] == True and i["created_at"] == d]                  
      else:
          posts_ids = [i["id"] for i in posts if i["comments_analysed"] == False and i["downloaded"] == True]    
    facebook_posts = []
    for p in posts_ids:    
        post_row = db["facebook_posts"].find({ "status_id" : p, "user_nickname" : nickname})
        for g in post_row:
            facebook_posts.append(g)

    count_facebook_posts = len(facebook_posts)

    raw_facebook, raw_comments = "", ""
    raw_positivereacs, raw_negativereacs, raw_neutralreacs = "", "", ""  
    positivereacs_number, negativereacs_number, neutralreacs_number = 0, 0, 0
    
    comments_dic = {"photo" : 0, "video" : 0, "link" : 0, "status" : 0, "note" : 0, "event" : 0}
    shares_dic = {"photo" : 0, "video" : 0, "link" : 0, "status" : 0, "note" : 0, "event" : 0}   
    reactions_dic = {"photo" : 0, "video" : 0, "link" : 0, "status" : 0, "note" : 0, "event" : 0}
    likes_dic = {"photo" : 0, "video" : 0, "link" : 0, "status" : 0, "note" : 0, "event" : 0}
    loves_dic = {"photo" : 0, "video" : 0, "link" : 0, "status" : 0, "note" : 0, "event" : 0}
    wows_dic = {"photo" : 0, "video" : 0, "link" : 0, "status" : 0, "note" : 0, "event" : 0}
    hahas_dic = {"photo" : 0, "video" : 0, "link" : 0, "status" : 0, "note" : 0, "event" : 0}
    angrys_dic = {"photo" : 0, "video" : 0, "link" : 0, "status" : 0, "note" : 0, "event" : 0}
    sads_dic = {"photo" : 0, "video" : 0, "link" : 0, "status" : 0, "note" : 0, "event" : 0}
    
    shares_number = 0
    for f in facebook_posts:
        status_id = f["status_id"]
        status_type = f["status_type"] if f["status_type"] else ""
        comments_number += f["status_num_comments"] if f["status_num_comments"] else 0
        shares_number += f["status_num_shares"] if f["status_num_shares"] else 0
        reactions_number += f["status_num_reactions"] if f["status_num_reactions"] else 0
        raw_facebook += f["status_message"] if f["status_message"] else ""
        
        #Store comments, shares and reactions
        comments_dic[status_type] += f["status_num_comments"]
        shares_dic[status_type] += f["status_num_shares"]
        reactions_dic[status_type] += f["status_num_reactions"]
        likes_dic[status_type] += f["status_num_likes"]
        loves_dic[status_type] += f["status_num_loves"]
        wows_dic[status_type] += f["status_num_wows"]
        hahas_dic[status_type] += f["status_num_hahas"]
        angrys_dic[status_type] += f["status_num_angrys"]
        sads_dic[status_type] += f["status_num_sads"]
        
        print("->Post:" + colored.green(f["status_message"]))
        print("  Tipo de status:" + colored.blue(f["status_type"]))
        print("  Numero de comentarios:" + colored.blue(f["status_num_comments"]))
        print("  Numero de compartilhamentos:" + colored.blue(f["status_num_shares"]))
        print("  Numero de reacoes:" + colored.blue(f["status_num_reactions"]))

        count = 0
        for x in f["comments"]:
            if count <= answer:
                raw_comments += x["message"] 
                print("->Comentario Analisado:" + colored.green(x["message"]))
                comment_reaction = analyse_reaction(x["message"])
                if comment_reaction.values()[0] == "positive":
                    raw_positivereacs += x["message"]       
                    human_interactions.insert({"comment_like_count" : x["like_count"],  "author_name" : x["from"]["name"], "author_id" : x["from"]["id"], "admin_nickname" : "", "comment_text" : x["message"], "social_network" : "facebook", "reaction_type" : "positive", "user_nickname" : nickname, "already_validated" : False, "is_right" : False})                
                    positivereacs_number += 1
                    print("  Reacao:" + colored.blue("Positiva"))
                elif comment_reaction.values()[0] == "negative":
                    raw_negativereacs += x["message"]  
                    human_interactions.insert({"comment_like_count" : x["like_count"], "author_name" : x["from"]["name"], "author_id" : x["from"]["id"], "admin_nickname" : "", "comment_text" : x["message"], "social_network" : "facebook", "reaction_type" : "negative", "user_nickname" : nickname, "already_validated" : False, "is_right" : False})     
                    negativereacs_number += 1
                    print("  Reacao:" + colored.red("Negativa"))
                else:
                    raw_neutralreacs += x["message"]   
                    human_interactions.insert({"comment_like_count" : x["like_count"], "author_name" : x["from"]["name"], "author_id" : x["from"]["id"], "admin_nickname" : "", "comment_text" : x["message"], "social_network" : "facebook", "reaction_type" : "neutral", "user_nickname" : nickname, "already_validated" : False, "is_right" : False})    
                    neutralreacs_number += 1
                    print("  Reacao:" + colored.yellow("Neutra"))
                count += 1
            else:
                break     
        db["job_stacks"].update({"user_nickname" : nickname, "social_network" : "facebook", "posts.id" : status_id}, {"$set" : { "posts.$.comments_analysed" : True }})
    
    average_comments, average_shares, average_reactions, average_positivereacs, average_negativereacs, average_neutralreacs = 0, 0, 0, 0, 0, 0
    if comments_number:
        #average_comments_count, average_reactions_count, average_likes_count, average_sads_count, average_hahas_count, average_angrys_count, average_wows_count, average_positivereacs_count, average_negativereacs_count, average_neutralreacs_count       
        average_comments = round(comments_number / facebook_count)
        average_reactions = round(reactions_number / facebook_count)
        average_shares = round(shares_number / facebook_count)
        average_positivereacs = round((positivereacs_number / comments_number) * 100)
        average_negativereacs = round((negativereacs_number / comments_number) * 100)
        average_neutralreacs = round((neutralreacs_number / comments_number) * 100)   
        print(colored.blue("->Numero medio de comentarios por post:") + str(average_comments))
        print(colored.blue("  Numero de posts no facebook:") + facebook_count )
        print(colored.blue("  Numero total de comentarios:") + comments_number)    
        print(colored.blue("  Numero de comentarios positivos:") + str(positivereacs_number) + "(" + str(average_positivereacs)  + "%)")
        print(colored.blue("  Numero de comentarios negativos:") + str(negativereacs_number) + "(" + str(average_negativereacs)  + "%)")
        print(colored.blue("  Numero de comentarios neutros:") + str(neutralreacs_number) + "(" + str(average_neutralreacs)  + "%)")
        print(colored.blue("->Numero medio de reacoes por post:") + str(average_reactions ))    
    
    if raw_facebook:
        #Posts
        fdist_posts = return_topwords(raw_facebook) if return_topwords(raw_facebook) else []
        print("Palavras mais comuns nos posts:" + colored.red(fdist_posts.most_common(50)))
        print("Palavra mais comum nos posts:" + colored.red(fdist_posts.max()))   
           
    if raw_comments:
        #Comentarios
        fdist_comments = return_topwords(raw_comments) if return_topwords(raw_comments) else []
        print("Palavras mais comuns nos comentarios:" + colored.red(fdist_comments.most_common(50)))
        print("Palavra mais comum nos comentarios:" + colored.red(fdist_comments.max())) 
    
    if raw_positivereacs:
        #palavras mais comuns nos retweets positivos
        fdist = return_topwords(raw_positivereacs) if return_topwords(raw_positivereacs) else []
        print("Palavras mais comuns nos comentarios positivos:" + colored.red(fdist.most_common(50)))
        print("Palavra mais comum nos comentarios positivos:" + colored.red(fdist.max())) 
    
    #comentarios positivos mais votados
    if raw_negativereacs:
        #palavras mais comuns nos retweets negativos 
        fdist = return_topwords(raw_negativereacs) if return_topwords(raw_negativereacs) else []
        print("Palavras mais comuns nos comentarios negativos:" + colored.red(fdist.most_common(50)))
        print("Palavra mais comum nos comentarios negativos:" + colored.red(fdist.max())) 
    
    """
    ->Store the influence of each post type
    """
    #video
    video_popularity = round((comments_dic["video"] + reactions_dic["video"] + shares_dic["video"]) / video_count) if video_count else 0

    #photo
    photo_popularity = round((comments_dic["photo"] + reactions_dic["photo"] + shares_dic["photo"]) / photo_count) if photo_count else 0
    
    #link
    link_popularity = round((comments_dic["link"] + reactions_dic["link"] + shares_dic["link"]) / link_count) if link_count else 0

    #status
    status_popularity = round((comments_dic["status"] + reactions_dic["status"] + shares_dic["status"]) / status_count) if status_count else 0
    
    if raw_comments:
        if mode == "partial":   
            ready_statistics.insert({"average_shares_count" : average_shares, "average_comments_count" : average_comments, "average_reactions_count" : average_reactions, "sads_distribution" : sads_dic, "angrys_distribution" : angrys_dic, "hahas_distribution" : hahas_dic, "wows_distribution" : wows_dic, "likes_distribution" : likes_dic, "loves_distribution" : loves_dic, "reactions_distribution" : reactions_dic, "shares_distribution" : shares_dic, "comments_distribution" : comments_dic, "shares_count" : shares_number, "comments_count" : comments_number, "first_statistic" : False, "status_type_popularity" : {"video" : video_popularity, "photo" : photo_popularity, "link" : link_popularity, "status" : status_popularity}, "fans_country_distribution" : countries_distribution_dic, "comments_average_neutralreacs" : average_neutralreacs, "comments_average_negativereacs" : average_negativereacs, "comments_average_positivereacs" : average_positivereacs , "status_count" : status_count, "percentage_status" : p_status, "link_count" : link_count, "percentage_links" : p_links, "video_count" : video_count, "percentage_videos" : p_videos, "photo_count" : photo_count, "percentage_photos" : p_photos, "words_fdist_posts" : fdist_posts, "words_fdist_comments" : fdist_comments, "created_at" : time.strftime("%Y-%m-%d"), "social_network" : "facebook", "user_nickname" : nickname, "posts_count" : facebook_count })
        else:
            ready_statistics.insert({"average_shares_count" : average_shares, "average_comments_count" : average_comments, "average_reactions_count" : average_reactions, "sads_distribution" : sads_dic, "angrys_distribution" : angrys_dic, "hahas_distribution" : hahas_dic, "wows_distribution" : wows_dic, "likes_distribution" : likes_dic, "loves_distribution" : loves_dic, "reactions_distribution" : reactions_dic, "shares_distribution" : shares_dic, "comments_distribution" : comments_dic, "shares_count" : shares_number, "comments_count" : comments_number, "first_statistic" : True, "status_type_popularity" : {"video" : video_popularity, "photo" : photo_popularity, "link" : link_popularity, "status" : status_popularity}, "fans_country_distribution" : countries_distribution_dic, "comments_average_neutralreacs" : average_neutralreacs, "comments_average_negativereacs" : average_negativereacs, "comments_average_positivereacs" : average_positivereacs , "status_count" : status_count, "percentage_status" : p_status, "link_count" : link_count, "percentage_links" : p_links, "video_count" : video_count, "percentage_videos" : p_videos, "photo_count" : photo_count, "percentage_photos" : p_photos, "words_fdist_posts" : fdist_posts, "words_fdist_comments" : fdist_comments, "created_at" : time.strftime("%Y-%m-%d"), "social_network" : "facebook", "user_nickname" : nickname, "posts_count" : facebook_count })           
    else:
        if mode == "partial":
            ready_statistics.insert({"average_shares_count" : average_shares, "average_comments_count" : average_comments, "average_reactions_count" : average_reactions, "sads_distribution" : sads_dic, "angrys_distribution" : angrys_dic, "hahas_distribution" : hahas_dic, "wows_distribution" : wows_dic, "likes_distribution" : likes_dic, "loves_distribution" : loves_dic, "reactions_distribution" : reactions_dic, "shares_distribution" : shares_dic, "comments_distribution" : comments_dic, "shares_count" : shares_number, "comments_count" : comments_number, "first_statistic" : False, "status_type_popularity" : {"video" : video_popularity, "photo" : photo_popularity, "link" : link_popularity, "status" : status_popularity}, "fans_country_distribution" : countries_distribution_dic, "comments_average_neutralreacs" : 0, "comments_average_negativereacs" : 0, "comments_average_positivereacs" : 0 , "status_count" : status_count, "percentage_status" : p_status, "link_count" : link_count, "percentage_links" : p_links, "video_count" : video_count, "percentage_videos" : p_videos, "photo_count" : photo_count, "percentage_photos" : p_photos, "words_fdist_posts" : [], "words_fdist_comments" : [], "created_at" : time.strftime("%Y-%m-%d"), "social_network" : "facebook", "user_nickname" : nickname, "posts_count" : facebook_count })
        else:
            ready_statistics.insert({"average_shares_count" : average_shares, "average_comments_count" : average_comments, "average_reactions_count" : average_reactions, "sads_distribution" : sads_dic, "angrys_distribution" : angrys_dic, "hahas_distribution" : hahas_dic, "wows_distribution" : wows_dic, "likes_distribution" : likes_dic, "loves_distribution" : loves_dic, "reactions_distribution" : reactions_dic, "shares_distribution" : shares_dic, "comments_distribution" : comments_dic, "shares_count" : shares_number, "comments_count" : comments_number, "first_statistic" : True, "status_type_popularity" : {"video" : video_popularity, "photo" : photo_popularity, "link" : link_popularity, "status" : status_popularity}, "fans_country_distribution" : countries_distribution_dic, "comments_average_neutralreacs" : 0, "comments_average_negativereacs" : 0, "comments_average_positivereacs" : 0 , "status_count" : status_count, "percentage_status" : p_status, "link_count" : link_count, "percentage_links" : p_links, "video_count" : video_count, "percentage_videos" : p_videos, "photo_count" : photo_count, "percentage_photos" : p_photos, "words_fdist_posts" : [], "words_fdist_comments" : [], "created_at" : time.strftime("%Y-%m-%d"), "social_network" : "facebook", "user_nickname" : nickname, "posts_count" : facebook_count })                
    print("________________________________________________")

"""
-> Retorna a distribuicao do publico por sexo, idade e paises
https://developers.facebook.com/docs/graph-api/reference/v2.7/insights

http://blog.alejandronolla.com/2013/05/15/detecting-text-language-with-python-and-nltk/

def show_public_distribution():
"""

def describe_fields(db):
    #facebook_posts = db["facebook_posts"].find()
    #twitter_posts = db["twitter_posts"].find() 
    
    facebook_posts = db["twitter_posts"].find_one({"user_nickname" : "felipeguerrads"})
    print(facebook_posts)
    #print(colored.blue( ", ".join(facebook_posts.keys())))
    
    #print(colored.red( ", ".join(twitter_posts[0].keys())))
    
def retweets_list(db):
    """
    keys:    
    [u'contributors', u'truncated', u'text', u'is_quote_status', u'in_reply_to_status_id', u'id', u'favorite_count', u'source', u'retweeted', u'coordinates', u'entities', u'in_reply_to_screen_name', u'id_str', u'retweet_count', u'in_reply_to_user_id', u'favorited', u'retweeted_status', u'user', u'geo', u'in_reply_to_user_id_str', u'possibly_sensitive', u'lang', u'created_at', u'in_reply_to_status_id_str', u'place']
    """
    twitter_post = db["twitter_posts"].find()[10] 
    twitter = twitter_post["retweets_list"][0]
    print(colored.yellow(twitter.keys()))

"""
This approach works fine with wel written texts (without grammatical errors)
and not small texts
"""
def identify_language(text):
    text = preprocess.convert2unicode(text)
    tokens = wordpunct_tokenize(text)
    words = [word.lower() for word in tokens]    
    languages_ratios = {}
    for language in stopwords.fileids():
        stopwords_set = set(stopwords.words(language))
        words_set = set(words)
        common_elements = words_set.intersection(stopwords_set)
        languages_ratios[language] = len(common_elements)
    most_rated_language = max(languages_ratios, key = languages_ratios.get)
    return most_rated_language
"""
->Other technique for guessing the text's language is using N-GRAM-Based text
categorization (useful when the syntax rules are not being followed, also useful
for identifying the topic of the text and not just language)
"""

if __name__ == "__main__":
    settings.environment()
    client = MongoClient()    
    db = client[settings.database_name]
    describe_fields(db)
    """    
    short_language = settings.short_language    
    if os.path.isdir(short_language + "_train_model"):
        os.chdir(short_language + "_train_model")    
    else:
        os.mkdir(short_language + "_train_model")
        os.chdir(short_language + "_train_model")
    folders = glob.glob("*")
    counter = 0    
    for f in folders:
        os.chdir(f)
        files = glob.glob("*.txt")
        for g in files:
            if(g.find(time.strftime("%d:%m:%Y"))) != -1:
                counter += 1
            else:
                continue
        os.chdir("..")
    os.chdir("..")
    if counter != len(folders) or counter == 0 or len(folders) == 0:
        feed_trainmodel()    
   """
    #feed_trainmodel()
    #analyse_twitter(db)
    #analyse_facebook(db)
    
    #print(analyse_reaction(["cara, que comentario imbecil", "noosa velho adoro voce", "odeio muito o que voce faz"]))
    
 