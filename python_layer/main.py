# -*- encoding: utf-8 -*-
import facebook_layer.get_fb_posts_fb_page
import facebook_layer.get_fb_comments_from_fb
import settings, preprocess
import json
import  sys
import datetime
from clint.textui import colored
import tweepy
import twitter.main
import facebook_layer.main
from tweepy import OAuthHandler
from instagram.client import InstagramAPI
from pymongo import MongoClient
import time
from slacker import Slacker

#Set the encoding
reload(sys)
sys.setdefaultencoding('UTF8')

"""
Integrates all social networks
->Predict social reactions
"""

def get_fbposts(db, nickname, page_id, access_token, mode = "total", ids_list = [], is_premium = False, slack = object):
    
    """
    Get reactions separately
    1)
    https://developers.facebook.com/docs/graph-api/reference/post/reactions
    
    2)
    http://stackoverflow.com/questions/35606109/getting-facebook-reactions-with-graph-api
    """

    if not slack:
        settings.slack_credentials()
        slack_api_token = settings.api_token
        slack = Slacker(slack_api_token)

    about_collections = db["about_collections"]
    results = about_collections.find({"social_network" : "facebook", "user_nickname" : nickname})
    if results.count() == 0:
        profile_info = facebook_layer.main.retrieve_profileinfo(page_id, access_token)
        profile_info["user_nickname"] = nickname
        about_collections.insert(profile_info)
    else:
        #add likes to list
        like_count = facebook_layer.main.get_fb_page_like_count(page_id, access_token)
        db["about_collections"].update({"user_nickname" : nickname, "social_network" : "facebook"}, { "$push" : {"fan_count_list" : { time.strftime("%Y-%m-%d") : like_count }}})
        
        #add talking about to list
        talking_about_count = facebook_layer.main.get_fb_page_talking_about_count(page_id, access_token)
        db["about_collections"].update({"user_nickname" : nickname, "social_network" : "facebook"}, { "$push" : {"talking_about_count_list" : { time.strftime("%Y-%m-%d") : talking_about_count }}})

        print(colored.green("Informacoes de perfil do facebook adicionadas anteriormente"))
  
    posts_to_download = 0
    if mode == "total":    
        posts_list = db["job_stacks"].find({"social_network" : "facebook", "user_nickname" : nickname})
        statuses = []
        for p in posts_list:
            posts_to_download += len(p["posts"])
            for x in p["posts"]:
                if x["downloaded"] == False and x["comments_analysed"] == False:
                    status = facebook_layer.get_fb_posts_fb_page.getFacebookPagePostById(page_id, access_token, x["id"])
                    statuses.append(status)
                else:
                    continue
    else:
        statuses = []
        posts_to_download += len(ids_list)
        for x in ids_list:
            status = facebook_layer.get_fb_posts_fb_page.getFacebookPagePostById(page_id, access_token, x)
            statuses.append(status)

    collection = db["facebook_posts"]
    posts_already_downloaded = 1
    for status in statuses:
        if 'reactions' in status:
            t0 = time.time()
            """
            {u'reactions': {u'data': [], u'summary': {u'total_count': 153746, u'viewer_reaction': u'NONE'}}, u'name': u'Timeline Photos', u'comments': {u'data': [], u'summary': {u'total_count': 2409, u'can_comment': False, u'order': u'ranked'}}, u'link': u'https://www.facebook.com/BillGates/photos/a.10150331291841961.334784.216311481960/10153668328671961/?type=3', u'shares': {u'count': 2423}, u'created_time': u'2016-06-19T14:14:01+0000', u'message': u'Happy Father\u2019s Day to one of the most generous and wise people I know. \n\nThanks, Dad, for all your love.', u'type': u'photo', u'id': u'216311481960_10153668328671961'}
            {u'reactions': {u'data': [], u'summary': {u'total_count': 19046, u'viewer_reaction': u'NONE'}}, u'name': u'Iceland Carbon Dioxide Storage Project Locks Away Gas, and Fast', u'comments': {u'data': [], u'summary': {u'total_count': 826, u'can_comment': False, u'order': u'ranked'}}, u'link': u'http://b-gat.es/1UC3Wjv', u'shares': {u'count': 1681}, u'created_time': u'2016-06-18T15:41:01+0000', u'message': u'There are some pretty cool ideas out there for carbon capture and storage. \n\nThis new approach could lock CO2 gases away forever.', u'type': u'link', u'id': u'216311481960_10153666109506961'}
            """
            status_id = status["id"]
            status_message = preprocess.convert2unicode(status["message"]) if "message" in status else ""
            status_type = status["type"] if "type" in status else ""
            status_link = status["link"] if "link" in status else ""    
            status_created_time = status["created_time"] if "created_time" in status else ""
            status_num_reactions = status["reactions"]["summary"]["total_count"] if "reactions" in status else 0
            status_num_comments = status["comments"]["summary"]["total_count"] if "comments" in status  else 0
            status_num_shares = status["shares"]["count"]  if "shares" in status else 0
            
            results = collection.find({"status_id" : status_id, "user_nickname" : nickname})
            if results.count() == 0: 
                status_message2 = preprocess.process(status_message)        
                status_hashtags_list2 = preprocess.get_hashtags(status_message2)
                
                #"reactions" : { "love" : { "data" : [ ], "summary" : { "total_count" : 0 } }, "like" : { "data" : [ ], "summary" : { "total_count" : 3403 } }, "wow" : { "data" : [ ], "summary" : { "total_count" : 0 } }, "haha" : { "data" : [ ], "summary" : { "total_count" : 0 } }, "sad" : { "data" : [ ], "summary" : { "total_count" : 0 } }, "id" : "216311481960_10151447409741961", "angry" : { "data" : [ ], "summary" : { "total_count" : 0 } } } }
                reactions = facebook_layer.get_fb_posts_fb_page.getReactionsForStatus(status_id, access_token)
                status_num_loves = reactions["love"]["summary"]["total_count"]
                status_num_likes = reactions["like"]["summary"]["total_count"]
                status_num_wows = reactions["wow"]["summary"]["total_count"]
                status_num_hahas = reactions["haha"]["summary"]["total_count"]
                status_num_sads = reactions["sad"]["summary"]["total_count"]
                status_num_angrys = reactions["angry"]["summary"]["total_count"]
                
                #get comments               
                if is_premium == True:
                    comments = facebook_layer.get_fb_comments_from_fb.getFacebookCommentFeedData(status_id, access_token, 100000)
                else:
                    comments = facebook_layer.get_fb_comments_from_fb.getFacebookCommentFeedData(status_id, access_token, 20)                
                for c in comments:
                    comment_created_time = c["created_time"]
                    comment_created_date = comment_created_time.split("T")[0]
                    comment_created_hour = comment_created_time.split("T")[1].split("+")[0]
                    c["created_hour"] = comment_created_hour
                    c["created_date"] = comment_created_date
                #{"status_created_time" : "2011-11-03T16:03:56+0000"}
                #Importante pois indica a melhor hora para postar e o tempo de reacao nos comentarios
                status_created_date = status_created_time.split("T")[0]
                status_created_hour = status_created_time.split("T")[1].split("+")[0] 
                collection.insert({ "status_num_angrys" : status_num_angrys, "status_num_sads" : status_num_sads, "status_num_hahas" : status_num_hahas, "status_num_wows" : status_num_wows, "status_num_likes" : status_num_likes, "status_num_loves" : status_num_loves, "user_nickname" : nickname, "status_id" : status_id, "comments" : comments, "status_type" : status_type, "status_message" : status_message, "status_link" : status_link, "status_created_date" : status_created_date, "status_created_hour" : status_created_hour, "status_num_reactions" : status_num_reactions, "status_num_comments" : status_num_comments, "status_num_shares" : status_num_shares,  "status_hashtags" : status_hashtags_list2 })
                print("->Post:" + str(colored.green(status_message)))
                print("  Contagem:" + str(colored.blue( str(posts_already_downloaded)  + "/" + str(posts_to_download))))
                
                if posts_already_downloaded % 20 == 0:
                   slack.chat.post_message("#user_" + nickname, "Download counter:" + str(posts_already_downloaded) + "/" + str(posts_to_download)) 

                print("  Tipo do post:" + str( colored.blue(status_type)))
                print("  Link do post:" + str( colored.blue(status_link)))   
                print("  Data da postagem:" + str( colored.blue( status_created_time )))
                print("  Numero de compartilhamentos:" + str(colored.blue(status_num_shares)))
                print("  Numero de comentarios:" + str(colored.blue(status_num_comments)))
                print("  Numero de reacoes:" + str(colored.blue(status_num_reactions))) 
                
                #Mudar a data para o dia de hoje?
                db["job_stacks"].update({"user_nickname" : nickname, "social_network" : "facebook", "posts.id" : status_id}, {"$set" : { "posts.$.downloaded" : True }})
                 
                #Adicionar human_interactions
                
                if status_hashtags_list2:
                    print("  Hashtags:" + colored.blue(" ".join(status_hashtags_list2)))
                #Reacao negativa > 
                if status_num_angrys + status_num_sads > status_num_loves + status_num_hahas:
                    print(colored.red("  Reacao negativa"))
                elif status_num_angrys + status_num_sads == status_num_loves + status_num_hahas:
                    print(colored.blue("  Reacao neutra"))
                else:
                    print(colored.green("  Reacao positiva"))
                posts_already_downloaded += 1
                tf = time.time()
                dt = round(tf-t0)
                db["posts_timer"].insert({ "user_nickname" : nickname, "social_network" : "facebook", "status_type" : status_type, "total_time" : dt, "status_message_length" : len(status_message), "status_comments_number" : status_num_comments })               
            else:
                continue

def get_twposts(db, is_premium, nickname, scr_name, consumer_key, consumer_secret, access_token, access_secret, user_id, slack):
    about_collections = db["about_collections"]
    results = about_collections.find({"social_network" : "twitter", "user_nickname" : nickname})
    if results.count() == 0:
        profile_info = twitter.main.retrieve_profileinfo(consumer_key, consumer_secret, access_token, access_secret, user_id)           
        profile_info["user_nickname"] = nickname
        about_collections.insert(profile_info)
    else:
        followers_count = twitter.main.get_followers_count(consumer_key, consumer_secret, access_token, access_secret, user_id)    
        db["about_collections"].update({"user_nickname" : nickname, "social_network" : "twitter"}, { "$push" : {"followers_count_list" : { time.strftime("%Y-%m-%d") : followers_count }}} )
        print(colored.green("Informacoes de perfil do twitter adicionadas anteriormente"))
      
    collection = db["twitter_posts"]    
    auth = OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_secret)
    api = tweepy.API(auth, wait_on_rate_limit = True, wait_on_rate_limit_notify = True)
    
    slack.chat.post_message("#user_" + nickname, "ANSWER THE QUESTION") 
    answer = raw_input("Digite o numero de tweets que deseja baixar:")
    answer = int(answer)
    #When nothing is especified, the number of posts downlowaded will be 20 
    status_list = api.user_timeline(screen_name = scr_name, include_rts = True, count = 10000)
    count = 1
    for status in status_list:
        if count <= answer:
            t0 = time.time()
            hashtags_list, mentions_list = [], []
            """
            {u'contributors': None, u'truncated': False, u'text': u'@RSherman_25 \U0001f440', u'is_quote_status': False, u'in_reply_to_status_id': 735311433072709637, u'id': 735330843007328258, u'favorite_count': 170, u'source': u'<a href="http://twitter.com/download/iphone" rel="nofollow">Twitter for iPhone</a>', u'retweeted': False, u'coordinates': None, u'entities': {u'symbols': [], u'user_mentions': [{u'id': 27698202, u'indices': [0, 12], u'id_str': u'27698202', u'screen_name': u'RSherman_25', u'name': u'Richard Sherman'}], u'hashtags': [], u'urls': []}, u'in_reply_to_screen_name': u'RSherman_25', u'in_reply_to_user_id': 27698202, u'retweet_count': 23, u'id_str': u'735330843007328258', u'favorited': False, u'user': {u'follow_request_sent': False, u'has_extended_profile': False, u'profile_use_background_image': True, u'default_profile_image': False, u'id': 198975780, u'profile_background_image_url_https': u'https://pbs.twimg.com/profile_background_images/564884921798254593/lOR1ATM9.jpeg', u'verified': True, u'profile_text_color': u'333333', u'profile_image_url_https': u'https://pbs.twimg.com/profile_images/452246563155230720/H6fVrGy9_normal.jpeg', u'profile_sidebar_fill_color': u'F5F5F5', u'entities': {u'url': {u'urls': [{u'url': u'http://t.co/khbd6TxFzL', u'indices': [0, 22], u'expanded_url': u'http://Jordan.com', u'display_url': u'Jordan.com'}]}, u'description': {u'urls': []}}, u'followers_count': 2756119, u'profile_sidebar_border_color': u'000000', u'id_str': u'198975780', u'profile_background_color': u'050505', u'listed_count': 5167, u'is_translation_enabled': False, u'utc_offset': -25200, u'statuses_count': 7744, u'description': u'Soul. Performance. Style.    #WEAREJORDAN', u'friends_count': 80, u'location': u'', u'profile_link_color': u'FF374D', u'profile_image_url': u'http://pbs.twimg.com/profile_images/452246563155230720/H6fVrGy9_normal.jpeg', u'following': False, u'geo_enabled': True, u'profile_banner_url': u'https://pbs.twimg.com/profile_banners/198975780/1468704261', u'profile_background_image_url': u'http://pbs.twimg.com/profile_background_images/564884921798254593/lOR1ATM9.jpeg', u'screen_name': u'Jumpman23', u'lang': u'en', u'profile_background_tile': False, u'favourites_count': 401, u'name': u'Jordan', u'notifications': False, u'url': u'http://t.co/khbd6TxFzL', u'created_at': u'Tue Oct 05 18:27:05 +0000 2010', u'contributors_enabled': False, u'time_zone': u'Pacific Time (US & Canada)', u'protected': False, u'default_profile': False, u'is_translator': False}, u'geo': None, u'in_reply_to_user_id_str': u'27698202', u'lang': u'und', u'created_at': u'Wed May 25 04:45:09 +0000 2016', u'in_reply_to_status_id_str': u'735311433072709637', u'place': None}
            """
            status_json = status._json
            tweet_id = status_json["id"]
            results = collection.find({ "id" : tweet_id, "user_nickname" : nickname})
                
            if results.count() == 0:            
                text_splited = preprocess.process(status_json["text"])
                hashtags_list = preprocess.get_hashtags(text_splited)
                mentions_list = preprocess.get_mentions(text_splited)
                #Save the record on database
                status_json["hashtags"] = hashtags_list 
                status_json["user_nickname"] = nickname
                
                months_dic = {
                    "Jan" : "01",
                    "Feb" : "02",
                    "Mar" : "03",
                    "Apr" : "04",
                    "May" : "05",
                    "Jun" : "06",
                    "Jul" : "07", 
                    "Aug" : "08",
                    "Sep" : "09", 
                    "Oct" : "10", 
                    "Nov" : "11",
                    "Dec" : "12"
                }

                #Get RT's of a tweet                        
                retweets = api.retweets(tweet_id)           
                retweets_list = []
                for st in retweets:
                    retweet = st._json
                    rt_created_at = retweet["created_at"]
                    rt_created_month = rt_created_at.split()[1]
                    rt_created_day_of_month = rt_created_at.split()[2]
                    rt_created_hour = rt_created_at.split()[3]
                    rt_created_year = rt_created_at.split()[5]
                    rt_created_at = rt_created_year + "-" + months_dic[rt_created_month] + "-" + rt_created_day_of_month
                    retweet["created_at"] =  rt_created_at
                    retweet["created_at_hour"] = rt_created_hour
                    retweets_list.append(retweet)
                
                status_json["retweets_list"] = retweets_list
                
                #{ 'created_at': u'Tue Oct 05 18:27:05 +0000 2010' }
                #{ 'created_at' : u'Fri Aug 26 22:24:02 +0000 2016'}
                status_created_at = status_json["created_at"]                
                status_created_month = status_created_at.split()[1]
                status_created_day_of_month = status_created_at.split()[2]
                status_created_hour = status_created_at.split()[3]
                status_created_year = status_created_at.split()[5]

                #YYYY-MM-DD
                status_created_at = status_created_year + "-" + months_dic[status_created_month] + "-" + status_created_day_of_month
                status_json["created_at"] = status_created_at
                status_json["created_at_hour"] = status_created_hour
                if text_splited[0] == "RT":
                    print(colored.red("->ReTweet:") + colored.blue(status_json["text"]))
                    status_json["is_retweet"] = True
                else:
                    print(colored.red("->Tweet:") + colored.blue(status_json["text"]))  
                    status_json["is_retweet"] = False
                collection.insert(status_json)
                print("  Contagem:" + colored.blue(str(count) + "/" + str(answer)))
                print("  Numero de favoritos:" + colored.blue(status_json["favorite_count"]))       
                print("  Data:" + colored.blue(status_created_at))        
                if hashtags_list:
                    print("  Hashtags:" + colored.blue(" ".join(hashtags_list)))
                if mentions_list:
                    print("  Mencoes:" + colored.blue(" ".join(mentions_list)))
                tf = time.time()
                dt = round(tf - t0)
                db["posts_timer"].insert({ "user_nickname" : nickname, "social_network" : "twitter", "total_time" : dt, "status_message_length" : len(status_json["text"]), "status_retweets_number" : len(retweets_list) })               
                post_dic = {}
                post_dic["id"] = tweet_id
                post_dic["downloaded"] = True
                post_dic["comments_analysed"] = False
                post_dic["created_at"] = time.strftime("%Y-%m-%d")
                db["job_stacks"].update({"social_network" : "twitter", "user_nickname" : nickname}, { "$push" : { "posts" : post_dic}})
                count += 1
            else:
                continue
        else:
            break

"""
http://bobmckay.com/web/simple-tutorial-for-getting-an-instagram-clientid-and-access-token/
def get_igposts(db):
    settings.ig_credentials()
    #collection = db["instagram_posts"] 

    api = InstagramAPI(client_id = settings.client_id, client_secret = settings.client_secret)
    recent_media = api.user_recent_media(settings.user_id, 6)
    print(colored.yellow(recent_media))
    for media in recent_media:
        print media.images['standard_resolution'].url
"""    

def init():
    settings.environment()
    client = MongoClient()
    db = client[settings.database_name]
    
    get_fbposts(db)
    get_twposts(db)
    #get_igposts(db)
if __name__ == "__main__":
    init()