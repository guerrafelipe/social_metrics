import pdfkit
import operator
import time, sys, os
from pymongo import MongoClient
import settings, utilities
import pdf.total
from bs4 import BeautifulSoup as bs
import datetime
from datetime import date
from datetime import timedelta
import plotly.plotly as py
import plotly.graph_objs as go
from plotly.tools import FigureFactory as FF
from clint.textui import colored
from wordcloud import WordCloud
import matplotlib.pyplot as plt

# -*- encoding: utf-8 -*-
#Set the encoding
reload(sys)
sys.setdefaultencoding('UTF8')

def generate_pdf(db, nickname, mode = "total"):
  if mode == "total":
       pdf.total.initialize(db, nickname)
       """
       Scrap cover
       """
       pdf.total.scrap_cover(db, nickname)

       """
       Scrap Introduction
       """
       pdf.total.scrap_introduction(db, nickname)
   
       """
       Scrap Overview
       """
       pdf.total.scrap_overview(db, nickname)
   
       """
       Scrap Posts
       """
       pdf.total.scrap_posts(db, nickname)

       """
       Scrap Comments
       """
       pdf.total.scrap_comments(db, nickname)
       
       """
       Scrap other people like you
       """
       pdf.total.scrap_other_people_like_you(db, nickname)

       """
       Scrap Summary
       """
       pdf.total.scrap_summary(db, nickname)
       
       """
       Mount and save the pdf
       """
       file_name = "pdf/files/" + nickname + "_" + time.strftime("%Y:%m:%d") + "_firstreport" + ".pdf"
       options = {
        'encoding': "UTF-8",
        'title' : "Comigo-Estatistica Semanal"
       }
       pdfkit.from_file(
          [
          "html_pages/temporary/cover_" + nickname + ".html",
          "html_pages/total/toc.html",
          "html_pages/temporary/introduction_" + nickname + ".html",
          "html_pages/temporary/overview_" + nickname + ".html",
          "html_pages/temporary/posts_" + nickname + ".html",
          "html_pages/temporary/comments_" + nickname + ".html",
          "html_pages/temporary/other_profiles_" + nickname + ".html",
          "html_pages/temporary/summary_" + nickname + ".html"
          ],
          file_name,
          options = options,
       )
  elif mode == "partial":
    print("partial")
    
  else:
    print("Opcao nao existe")

def generate_ppt(db, nickname):
  print("Powerpoint generated")

def generate_excel(db, nickname):
  print("Excel Generated")

if __name__ == "__main__":
    settings.environment("production")
    client = MongoClient(settings.host)
    db = client[settings.database_name]
    
    #do_total_report(db, "marcoluque")
    generate_pdf(db, "marcoluque", "total")