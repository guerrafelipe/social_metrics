class User::RegistrationsController < Devise::RegistrationsController
# before_action :configure_sign_up_params, only: [:create]
# before_action :configure_account_update_params, only: [:update]

  # GET /resource/sign_up
   def new
     super
   end

  # POST /resource
  def create
      user_categories = ["Sports", "Music", "TV", "Literature", "Entertainment", "Beauty", "Religion", "Internet"]
      if user_categories.include? params[:user][:user_category]
        user = User.new(devise_registrations_permitted_parameters)
        if user.save
          user_id = User.where(nickname: params[:user][:nickname]).pluck(:_id)[0]
          user_object = User.find(user_id)
          user_object.update_attributes(is_admin: false, user_category: params[:user][:user_category])
          redirect_to "/dashboard/index"
        else
          flash[:error] = "Something went wrong"
          redirect_to "/signup"
        end
      else
        flash[:error] = "Your category is not listed"
        redirect_to "/signup"
      end
  end

  # GET /resource/edit
   def edit
     super
   end

  # DELETE /resource
  def destroy
    if current_user.is_admin?
      @user = User.find(params[:id])
      if @user.destroy
          redirect_to dashboard_manage_users_path, notice: "User deleted."
      end
    else
      redirect_to home_index_path
    end
  end
  
  # PUT /resource
  def update_profile
    fullname = params[:profile][:fullname]
    country = params[:profile][:country] 
    email = params[:profile][:email]
    user_id = User.where(nickname: current_user.nickname).pluck(:id)[0]
    user = User.find(user_id)
    user.update_attributes(fullname: fullname, country: country, email: email)
    flash[:notice] = "Social networks details updated successfully."
    redirect_to dashboard_manage_account_path
  end

  def manage_sn_accounts
    fb_page_link =  params[:manage_sn_accounts][:fb_page_link]
    tw_username = params[:manage_sn_accounts][:tw_username]
    tw_username = tw_username.sub("@", "")
    
    user_id = User.where(nickname: current_user.nickname).pluck(:id)[0]
    user = User.find(user_id)
    user.update_attributes(fb_page_link: fb_page_link, tw_scr_name: tw_username)
    flash[:notice] = "Social networks details updated successfully."
    redirect_to dashboard_manage_account_path
  end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
   def cancel
     super
   end

  protected
  # If you have extra params to permit, append them to the sanitizer.
   #def configure_sign_up_params
   #  devise_parameter_sanitizer.permit(:sign_up, keys: [:fullname, :email, :nickname,  :password, :password_confirmation])
   #end

  def devise_registrations_permitted_parameters
    params.require(:user).permit(:fullname, :country, :email, :nickname,  :password, :password_confirmation)
  end

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_account_update_params
  #   devise_parameter_sanitizer.permit(:account_update, keys: [:attribute])
  # end

  # The path used after sign up.
  # def after_sign_up_path_for(resource)
  #   super(resource)
  # end

  # The path used after sign up for inactive accounts.
  # def after_inactive_sign_up_path_for(resource)
  #   super(resource)
  # end
end
