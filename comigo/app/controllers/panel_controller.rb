class PanelController < ApplicationController
  
  before_filter :authenticate_user!
  before_filter :is_admin?  
  
  def index
  end

  def account
    user_id = User.where(nickname: current_user.nickname).pluck(:id)[0]
    @user = User.find(user_id)
  end

  def billing
  end

  def help
  end

  def myproducts
  end

  """
  Admin Pages
    db.users.update({'nickname':  'felipeguerrads'  }, {'$set': {'is_admin': true}})

    db.users.update({'nickname':  'felipeguerrads'  }, {'$set': {'tw_user_id': ""}})
    db.users.update({'nickname':  'felipeguerrads'  }, {'$set': {'fb_page_id': ""}})
  """
  #GET 
  def manageproducts
    @products = ["Cosmo"]
    @user_emails = User.all.pluck(:email)
  end
  
  #GET
  def manageusers
    @users = User.all
  end

  def trainai
    @users = User.where(is_admin: false)
    @human_interactions = HumanInteraction.all
    @human_interactions_validated = HumanInteraction.where(already_validated: true)
    @human_interactions_validated_count = HumanInteraction.where(already_validated: true).count
    if params[:nickname].to_s == "all"   
      @human_interactions_facebook = HumanInteraction.where(social_network: "facebook").limit(100)
      @human_interactions_twitter = HumanInteraction.where(social_network: "twitter").limit(100)
    else
      @human_interactions_facebook = HumanInteraction.where(social_network: "facebook", user_nickname: params[:nickname]).limit(100)
      @human_interactions_twitter = HumanInteraction.where(social_network: "twitter", user_nickname: params[:nickname]).limit(100)
    end
  end
  
  #POST
  def add_product_to_user
    user_id = User.where(email: params[:manageproduct][:user_email]).pluck(:id)[0]
    user_object = User.find(user_id)
    user_object.push(products: params[:manageproduct][:product_name])
    flash[:notice] = "Successfully added product to a user"
    redirect_to dashboard_manage_products_path
  end

  def manage_notifications
  end


  def is_admin?
  	if !current_user.is_admin?
  	    redirect_to home_index_path    
  	end
  end

end
