require 'open-uri'

class CosmoController < ApplicationController
  before_filter :authenticate_user!

  def index
    about_facebook = AboutCollection.where(:user_nickname => current_user.nickname, :social_network=> "facebook")        
    about_twitter = AboutCollection.where(:user_nickname => current_user.nickname, :social_network=> "twitter")

    if about_facebook.count == 0 or about_twitter.count == 0
        flash[:notice] = "Before, you should provide your social network's addresses"
        redirect_to "/dashboard/account"
    else      
        #Prepare informations         
        @about_facebook = AboutCollection.find(about_facebook.pluck(:_id)[0])  
        @about_twitter = AboutCollection.find(about_twitter.pluck(:_id)[0])

        #Ready Statistics
        ready_statistics_facebook = ReadyStatistic.where(:user_nickname => current_user.nickname, :social_network => "twitter")
        ready_statistics_twitter = ReadyStatistic.where(:user_nickname => current_user.nickname, :social_network => "twitter")
        
        if ready_statistics_twitter.count == 0 or ready_statistics_facebook.count == 0
            flash[:notice] = "Your social network's informations are being downloaded"
            redirect_to "/dashboard/account"
        else
            @ready_statistics_facebook = ReadyStatistic.find(ready_statistics_facebook.pluck(:_id)[0])
            @ready_statistics_twitter = ReadyStatistic.find(ready_statistics_twitter.pluck(:_id)[0])
            
            #Download facebook image
            #picture_url = @about_facebook.picture["url"]
            #user_nickname = current_user.nickname
            #out_file = File.new(Rails.root.join('public', 'socialnetworks_profilespic', user_nickname + "_facebookpic").to_s, "wb")
            #out_file.puts( open(picture_url).read )
            #@uploader = FacebookPicUploader.new
            #@uploader.store!(out_file)
            #out_file.close

            #Download instagram image
            #picture_url2 = @about_twitter.profile_image_url
            #out_file2 = File.new(Rails.root.join('public', 'socialnetworks_profilespic', user_nickname + "_twitterpic").to_s, "wb")
            #out_file2.puts( open(picture_url2).read )
            #@uploader2 = TwitterPicUploader.new
            #@uploader2.store!(out_file2)
            #out_file2.close
            
            #Likes evolution
            fan_count_list_fb = about_facebook.pluck(:fan_count_list)[0]
            data_table = GoogleVisualr::DataTable.new
        	data_table.new_column('string', 'Dates' )
        	data_table.new_column('number', 'Likes')
            like_evolution = []
            for f in fan_count_list_fb               
                like_evolution << [f.keys[0], f.values[0]]
            end 
            data_table.add_rows(like_evolution)
            option = { width: 500, height: 240, title: 'Likes evolution' }
        	@chart = GoogleVisualr::Interactive::AreaChart.new(data_table, option)


            #Talking about evolution
            fan_count_list_fb = about_facebook.pluck(:talking_about_count_list)[0]
            data_table3 = GoogleVisualr::DataTable.new
            data_table3.new_column('string', 'Dates' )
            data_table3.new_column('number', 'People talking about you')
            talking_about_evolution = []
            for f in fan_count_list_fb               
                talking_about_evolution << [f.keys[0], f.values[0]]
            end 
            data_table3.add_rows(talking_about_evolution)
            option3 = { width: 500, height: 240, title: 'Talking about you evolution' }
            @chart3 = GoogleVisualr::Interactive::AreaChart.new(data_table3, option3)


            #Followers Evolution
            fan_count_list_tw = about_twitter.pluck(:followers_count_list)[0]
            data_table2 = GoogleVisualr::DataTable.new
            data_table2.new_column('string', 'Dates' )
            data_table2.new_column('number', 'Followers')
            followers_evolution = []
            for f in fan_count_list_tw
                followers_evolution << [f.keys[0], f.values[0]]
            end
            data_table2.add_rows(followers_evolution)
            option2 = { width: 500, height: 240, title: 'Followers evolution' }
            @chart2 = GoogleVisualr::Interactive::AreaChart.new(data_table2, option2)

            #Gender distribution
        	#data_table2 = GoogleVisualr::DataTable.new
            #data_table2.new_column('string', 'Gender')
            #data_table2.new_column('number', 'Percentage')
            #data_table2.add_rows(2)
            #data_table2.set_cell(0, 0, 'Male')
            #data_table2.set_cell(0, 1, 11 )
            #data_table2.set_cell(1, 0, 'Female')
            #data_table2.set_cell(1, 1, 2  )
            #opts2   = { :width => 400, :height => 240, :title => 'Gender distribution', :is3D => true }
            #@chart2 = GoogleVisualr::Interactive::PieChart.new(data_table2, opts2)
            
            @facebook_posts_count = FacebookPost.where(:user_nickname => current_user.nickname).count
        end
    end

  end


  def facebook
    about_facebook = AboutCollection.where(:user_nickname => current_user.nickname, :social_network => "facebook")
    @about_facebook = AboutCollection.find(about_facebook.pluck(:_id)[0])  
    @posts_all = FacebookPost.where(:user_nickname => current_user.nickname)
    
    #Select only this month's posts
    posts_ids = []
    for p in @posts_all
        status_created_date = p.status_created_date
        status_created_date = status_created_date.split("-")
        
        if status_created_date[0] == "2016" and status_created_date[1] == "08"
            posts_ids << p._id
        end
    end
    
    @posts = []
    for j in posts_ids
        post = FacebookPost.find(j)
        @posts << post
    end
    
    """
    Pegar a ultima estatistica realizada, pois nao se realiza estatistica diariamente
    (apenas nas sextas)
    """
    ready_statistics_facebook = ReadyStatistic.where(:user_nickname => current_user.nickname, :social_network => "facebook")
    @ready_statistics_facebook = ReadyStatistic.find(ready_statistics_facebook.pluck(:_id)[0])
    countries_dic = @ready_statistics_facebook.fans_country_distribution["value"]
    
    @posts_number = @ready_statistics_facebook.posts_count 
    @comments_number = 0
    @reactions_number = 0
    
    @facebook_posts_comments = FacebookPost.where(:user_nickname => current_user.nickname).pluck(:status_num_comments) 
    for p in @facebook_posts_comments
       @comments_number += p
    end    

    @facebook_posts_reactions = FacebookPost.where(:user_nickname => current_user.nickname).pluck(:status_num_reactions)
    for x in @facebook_posts_reactions
       @reactions_number += x
    end

    @average_comments_number = @comments_number / @posts_number
    @average_reactions_number = @reactions_number / @posts_number
    
    #Likes by country
    data_table3 = GoogleVisualr::DataTable.new
    data_table3.new_column('string', 'Country')
    data_table3.new_column('number', 'Likes')
    data_table3.add_rows(countries_dic.count)
    count = 0 
    countries_dic.each do |key, value|
        data_table3.set_cell(count, 0, key)
        data_table3.set_cell(count, 1, value)
        count += 1
    end
    opts3 = { :width => 400, :height => 600, :title => 'Likes by country', vAxis: { title: 'Country', titleTextStyle: { color: 'red' } } }
    @chart3 = GoogleVisualr::Interactive::BarChart.new(data_table3, opts3)
    
    #Post's distribution
    data_table2 = GoogleVisualr::DataTable.new
    data_table2.new_column('string', 'Post')
    data_table2.new_column('number', 'Percentage')
    data_table2.add_rows(4)
    data_table2.set_cell(0, 0, 'Video')
    data_table2.set_cell(0, 1, @ready_statistics_facebook.video_count)
    data_table2.set_cell(1, 0, 'Photo')
    data_table2.set_cell(1, 1, @ready_statistics_facebook.photo_count)
    data_table2.set_cell(2, 0, 'Link')
    data_table2.set_cell(2, 1, @ready_statistics_facebook.link_count)
    data_table2.set_cell(3, 0, 'Status')
    data_table2.set_cell(3, 1, @ready_statistics_facebook.status_count)
    opts2   = { :width => 600, :height => 240, :title => "Post's distribution", :is3D => true }
    @chart2 = GoogleVisualr::Interactive::PieChart.new(data_table2, opts2)
    
    
    #Count number of likes by post type
    data_table4 = GoogleVisualr::DataTable.new
    data_table4.new_column('string', 'Type of post')
    data_table4.new_column('number', 'Popularity')
    data_table4.add_rows(4)
     
    data_table4.set_cell(0, 0, "Photo")
    data_table4.set_cell(0, 1,  @ready_statistics_facebook.status_type_popularity["photo"])
    data_table4.set_cell(1, 0, "Link")
    data_table4.set_cell(1, 1, @ready_statistics_facebook.status_type_popularity["link"])
    data_table4.set_cell(2, 0, "Video")
    data_table4.set_cell(2, 1, @ready_statistics_facebook.status_type_popularity["video"])
    data_table4.set_cell(3, 0, "Status")
    data_table4.set_cell(3, 1, @ready_statistics_facebook.status_type_popularity["status"])

    opts4 = { :width => 300, :height => 400, :title => 'Popularity by post type', vAxis: { title: 'Post Type', titleTextStyle: { color: 'red' } } }
    @chart4 = GoogleVisualr::Interactive::BarChart.new(data_table4, opts4)
  end

  def twitter
     @tweets = TwitterPost.where(:user_nickname => current_user.nickname)
  end
  def instagram
    @instagram = Instagram.user_recent_media("1913236063")
  end
end

