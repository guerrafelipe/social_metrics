class HumanInteractionsController < ApplicationController
    before_filter :authenticate_user!
    before_filter :is_admin?
    
	def update
		interaction_id  = params[:id]
		suggested_interaction = params[:update][:suggested_category].downcase
    	interaction = HumanInteraction.find(interaction_id)
    	interaction.update_attributes(admin_nickname:current_user.nickname, reaction_type: suggested_interaction, already_validated: true, is_right: true, changer_user_nickname: current_user.nickname)
    	flash[:notice] = "Interaction update successfully"
    	redirect_to "/panel/trainai"
	end

	def destroy
	 	hinteraction_id = params[:id]
	 	
	 	if HumanInteraction.where(_id:hinteraction_id).delete
	 		flash[:notice] = "Comment successfully droped"
	 		redirect_to "/panel/trainai"
	 	else
	 		flash[:notice] = "Error when trying to remove this comment"
	 		redirect_to "/panel/trainai"
	 	end
	end
  
  private
  def is_admin?
  	if !current_user.is_admin?
  	    redirect_to home_index_path    
  	end
  end

end
