class DashboardController < ApplicationController
  before_filter :authenticate_user!
  
  def index
  end

  def account
      user_id = User.where(nickname: current_user.nickname).pluck(:id)[0]
      @user = User.find(user_id)
  end

  def billing
  end

  def help
  end

  def myproducts
  end
end
