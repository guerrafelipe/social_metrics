
pdf.text "Facebook posts", :size => 30, :style => :bold
pdf.text  "User:" + current_user.nickname
pdf.text "Datetime:" + Time.now.to_s
pdf.text "Number of posts:" + @posts_all.count.to_s

pdf.move_down(30)
items = @posts_all.map do |item|
[
  item.status_message,
  item.status_num_reactions,
  item.status_num_comments,
  item.status_num_shares,
  item.status_created_date
]
end


pdf.table items, {:header => true} do |table|
  table.header = (["Message", "Reactions", "Comments", "Shares"])
  #table.row(0).font_style = :bold
  table.position = :center
end