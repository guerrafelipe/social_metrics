pdf.text "Twitter posts", :size => 30, :style => :bold
pdf.text  "User:" + current_user.nickname
pdf.text "Datetime:" + Time.now.to_s
pdf.text "Number of tweets" + @tweets.count.to_s

pdf.move_down(30)
items = @tweets.map do |item|
[
  item.text,
  item.favorite_count,
  item.retweet_count,
  item.created_at
]
end


pdf.table items, {:header => true} do |table|
  table.header = (["Message", "Reactions", "Comments", "Shares"])
  #table.row(0).font_style = :bold
  table.position = :center
end