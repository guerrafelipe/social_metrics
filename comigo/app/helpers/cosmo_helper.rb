module CosmoHelper

 	def order_num_likes(comments)
        #{id => number of likes}
        likes_dic = {}
        for c in comments
        	likes_dic[ c["id"] ] = c["like_count"]
        end
        likes_dic = Hash[likes_dic.sort_by {|k, v| v}.reverse]
        new_comments = []
        likes_dic.each do |key ,value|
            for x in comments
                if x["id"] == key
                    new_comments << x
                end
            end    
        end
        return new_comments
 	end

 	def order_num_favorites(retweets)
       #{id => number of favorites}
       favorites_dic = {}
       for r in retweets
          favorites_dic[ r["id"] ] = r["favorite_count"]
       end
       favourites_dic = Hash[favorites_dic.sort_by{|k,v| v}.reverse]
       new_comments = []
       favorites_dic.each do |key, value|
          for r in retweets
            if r["id"] == key
              new_comments << r
            end
          end
       end
       return new_comments
 	end


end
