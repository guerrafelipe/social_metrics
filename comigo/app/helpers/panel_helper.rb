module PanelHelper

	def filter_not_validated(human_interactions)
       not_validated = []
       for h in human_interactions
          if h.already_validated == false
          	not_validated << h
          end
       end
       return not_validated
	end

	def number_of_validated_comments(user_nickname)
		return HumanInteraction.where(user_nickname: user_nickname, admin_nickname: current_user.nickname, social_network: "facebook", already_validated: true).count
	end
    
    def total_number_of_comments(user_nickname)
    	return HumanInteraction.where(user_nickname: user_nickname, social_network: "facebook").count
    end

	def number_of_validated_retweets(user_nickname)
       return HumanInteraction.where(user_nickname: user_nickname, admin_nickname: current_user.nickname, social_network: "twitter", already_validated: true).count
	end
    
  def total_number_of_retweets(user_nickname)
    return HumanInteraction.where(user_nickname: user_nickname, social_network: "twitter").count
  end

  def filter_not_empty_users(users, social_network)
    not_empty_users = []
    for u in users
      human_interactions = HumanInteraction.where(user_nickname: u.nickname, social_network: social_network, already_validated: false)
      if !human_interactions.blank?
         not_empty_users << u
      end
    end
    return not_empty_users
  end

end
