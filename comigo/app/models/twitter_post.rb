class TwitterPost
  include Mongoid::Document

  field :created_at, type: String
  field :created_at_hour, type: String
  field :text, type: String
  field :favorite_count, type: Integer
  field :retweet_count, type: Integer
  field :place, type: String
  field :retweets_list, type: Array
end
