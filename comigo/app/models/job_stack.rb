class JobStack
  include Mongoid::Document
  field :status_id, type: String
  field :downloaded, type: String
  field :social_network, type: String
  field :user_nickname, type: String
end
