class ReadyStatistic
  include Mongoid::Document
  
  """
  field :fb_likes_evolution, type: Hash
  field :tw_followers_evolution, type: Hash
  field :fb_top_words, type: Array
  field :tw_top_words, type: Array
  field :fb_top_hashtags, type: Array
  field :fb_top_mentions, type: Hashtag
  field :tw_top_hashtags, type: Array
  field :tw_top_mentions, type: Array  
  """
  
  field :user_nickname, type: String
  field :social_network, type: String

  field :percentage_photos, type: Integer
  field :percentage_status, type: Integer
  field :percentage_links, type: Integer
  field :percentage_videos, type: Integer
  field :comments_average_neutralreacs, type: Integer
  field :comments_average_positivereacs, type: Integer
  field :comments_average_negativereacs, type: Integer
  field :status_count, type: Integer
  field :link_count, type: Integer
  field :posts_count, type: Integer
  field :photo_count, type: Integer
  field :video_count, type: Integer

  field :fans_country_distribution, type: Array  
  field :words_fdist_posts, type: Array  
  field :words_fdist_comments, type: Array
  field :status_type_popularity, type: Array

end
