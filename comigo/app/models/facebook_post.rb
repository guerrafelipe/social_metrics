class FacebookPost
  include Mongoid::Document
  
  field :status_id, type: Integer
  field :user_nickname, type: String
  field :status_link, type: String
  field :status_message, type: String
  field :status_type, type: String
  field :status_num_shares, type: Integer
  field :status_created_hour, type: String
  field :status_created_date, type: String
  field :status_num_comments, type: Integer
  field :status_num_reactions, type: Integer
  field :status_num_likes, type: Integer
  field :status_num_loves, type: Integer
  field :status_num_wows, type: Integer
  field :status_num_hahas, type: Integer
  field :status_num_angrys, type: Integer
  field :status_num_sads, type: Integer
  field :comments, type: Array

end
