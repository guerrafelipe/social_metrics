class AboutCollection
  include Mongoid::Document
  
  #Facebook
  field :picture, type: Hash
  field :description, type: Integer
  field :fan_count_list, type: Hash
  field :talking_about_count_list, type: Hash
  field :social_network, type: String
  field :user_nickname, type: String
  
  #Twitter
  field :id, type: Integer
  field :follow_request_sent, type: Boolean
  field :has_extended_profile, type: Boolean
  field :profile_use_background_image, type: Boolean
  field :default_profile_image, type: Boolean
  field :profile_background_image_url_https, type: String
  field :verified, type: Boolean
  field :profile_text_color, type: String
  field :profile_image_url_https, type: String
  field :profile_image_url, type: String
  field :profile_banner_url, type: String
  field :profile_sidebar_fill_color, type: String
  field :entities, type: Hash
  field :followers_count_list, type: Hash
  field :profile_sidebar_border_color, type: String
  field :id_str, type: String
  field :profile_background_color, type: String
  field :listed_count, type: Integer
  field :status, type: Hash
  field :name, type: String
  field :screen_name, type: String
  field :created_at, type: String
  field :time_zone, type: String

end
