class HumanInteraction
  include Mongoid::Document
  field :comment_text, type: String
  field :reaction_type, type: String
  field :user_nickname, type: String
  field :admin_nickname, type: String
  field :social_network, type: String
  field :changer_user_nickname, type: String
  field :is_right, type: Mongoid::Boolean, default: false
  field :already_validated, type: Mongoid::Boolean, default: false
end
