class User
  include Mongoid::Document
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :omniauthable
  

  ## Database authenticatable
  field :email,              type: String, default: ""
  field :encrypted_password, type: String, default: ""
  field :fullname, type: String
  field :nickname, type: String
  field :country, type: String
  field :is_admin, type: Boolean
  field :products, type: Array, default: []
  field :language, type: String, default: ""
  field :country, type: String, default: ""

  #Twitter info
  field :tw_scr_name, type: String, default: ""
  field :tw_user_id, type: String, default: ""

  #fb info
  field :fb_page_id, type: String, default: ""
  field :fb_page_link, type: String, default: ""
  field :user_category, type: String
  
  ## Recoverable
  field :reset_password_token,   type: String
  field :reset_password_sent_at, type: Time

  ## Rememberable
  field :remember_created_at, type: Time

  #omniauth
  field :provider, type: String
  field :uid, type: String
  
  ## Trackable
  field :sign_in_count,      type: Integer, default: 0
  field :current_sign_in_at, type: Time
  field :last_sign_in_at,    type: Time
  field :current_sign_in_ip, type: String
  field :last_sign_in_ip,    type: String

  validates_presence_of :email, :fullname, :nickname, :country
  validates_uniqueness_of :email, :nickname

  ## Confirmable
  # field :confirmation_token,   type: String
  # field :confirmed_at,         type: Time
  # field :confirmation_sent_at, type: Time
  # field :unconfirmed_email,    type: String # Only if using reconfirmable

  ## Lockable
  # field :failed_attempts, type: Integer, default: 0 # Only if lock strategy is :failed_attempts
  # field :unlock_token,    type: String # Only if unlock strategy is :email or :both
  # field :locked_at,       type: Time
end
