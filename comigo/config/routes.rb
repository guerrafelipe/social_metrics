Rails.application.routes.draw do
  get 'monitor/index'

  get 'dashboard/index'

  get 'dashboard/account'

  get 'dashboard/billing'

  get 'dashboard/help'

  get 'dashboard/myproducts'

  get 'cosmo/index'

  get 'panel/index'

  get 'home/index', :as=> :home_index
  #get "cosmo/facebook.pdf", :as => :generate_pdf
  
  get 'home/pricing'

  get 'home/about'

  get 'home/contact'

  get "home/privacy_policy"
  
  get "panel/manageusers"
  get "panel/manageproducts", :as => :dashboard_manage_products
  get "panel/account", :as => :dashboard_manage_account
  get "panel/billing"
  get "panel/help"
  
  get "panel/trainai/:nickname", to: "panel#trainai"
  get '/panel/trainai', to: redirect('/panel/trainai/all')

  get '/panel',  to: 'panel#index'
  get "/dashboard", to: "dashboard#index"
  
  get "/cosmo", to: "cosmo#index"
  get "cosmo/facebook"
  get "cosmo/twitter"
  get "cosmo/instagram"
  
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'home#index'
  
  post "panel/add_product_to_user", :as => :add_product_to_user
  
 

  post "panel/manage_notifications", :as => :manage_notifications

  #GAMBIARRA (DEVIA SER POST)
  get "human_interactions/destroy/:id" => "human_interactions#destroy", :as => :destroy_interaction
  post "human_interactions/update/:id" => "human_interactions#update", :as => :update_interaction

  devise_for :user, controllers: {
    sessions: 'user/sessions',
    registrations: 'user/registrations',
    omniauth_callbacks: "omniauth_callbacks"
  }
  
  resource :user

  devise_scope :user do
    get '/login', to: 'user/sessions#new'
  end
  
  devise_scope :user do
    get '/signup', to: 'user/registrations#new'
  end

  devise_scope :user do
    get "/logout" => "user/sessions#destroy", :as => :quit_session
  end

  devise_scope :user do
    post "/user/create" => "user/registrations#create", :as=> :create_user_path
  end

  devise_scope :user do
    post "/user/update_profile" => "user/registrations#update_profile", :as=> :manage_user_profile
  end

  devise_scope :user do
     post "/user/update_social_accounts" => "user/registrations#manage_sn_accounts" , :as => :manage_sn_accounts
  end


  devise_scope :user do
    post "/user/password/update" => "user/passwords#update", :as=> :manage_account
  end

  get "/panel/manageusers", :as => :dashboard_manage_users
  
  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
