require 'test_helper'

class DashboardControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get account" do
    get :account
    assert_response :success
  end

  test "should get billing" do
    get :billing
    assert_response :success
  end

  test "should get help" do
    get :help
    assert_response :success
  end

  test "should get myproducts" do
    get :myproducts
    assert_response :success
  end

end
